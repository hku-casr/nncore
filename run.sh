#!/bin/bash
for e in $(seq 5 -1 1)
do
  mkdir -p err-${e}
  cd err-${e}

  for idx in {3..13}
  do
    timeout 15m ../nncore.py tanh     --wE 5 --wF 11 -d ${idx} --approx_err ${e}.0       2> err_half_${idx}_tanh.log       &
    timeout 15m ../nncore.py sigmoid  --wE 5 --wF 11 -d ${idx} --approx_err ${e}.0       2> err_half_${idx}_sigmoid.log    &
    timeout 15m ../nncore.py d_tanh   --wE 5 --wF 11 -d ${idx} --approx_err ${e}.0       2> err_half_${idx}_dtanh.log      &
    timeout 15m ../nncore.py tanh     --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0       2> err_single_${idx}_tanh.log     &
    timeout 15m ../nncore.py sigmoid  --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0       2> err_single_${idx}_sigmoid.log  &
    timeout 15m ../nncore.py d_tanh   --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0       2> err_single_${idx}_dtanh.log    &
    ((idx%2==0)) && wait
  done

  for idx in 3 5 7 9 11 13
  do
    timeout 15m ../nncore.py tanh     --wE 5 --wF 11 -d ${idx} --approx_err ${e}.0 --odd 2> err_half_odd_${idx}_tanh.log      &
    timeout 15m ../nncore.py sigmoid  --wE 5 --wF 11 -d ${idx} --approx_err ${e}.0 --odd 2> err_half_odd_${idx}_sigmoid.log   &
    timeout 15m ../nncore.py d_tanh   --wE 5 --wF 11 -d ${idx} --approx_err ${e}.0 --odd 2> err_half_odd_${idx}_dtanh.log     &
    timeout 15m ../nncore.py tanh     --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0 --odd 2> err_single_odd_${idx}_tanh.log    &
    timeout 15m ../nncore.py sigmoid  --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0 --odd 2> err_single_odd_${idx}_sigmoid.log &
    timeout 15m ../nncore.py d_tanh   --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0 --odd 2> err_single_odd_${idx}_dtanh.log   &
    (((idx-1)%4==0)) && wait
  done

  cd ..
done
