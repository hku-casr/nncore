# NnCore
**N**erual **N**etwork arithmetic **CORE** generator (NnCore) is an floating-point arithmetic core generator 
for Neural Network activation-functions/operators, developed at EEE, HKU.

Floating-point format follows that of the [FloPoCo](http://flopoco.gforge.inria.fr/flopoco_user_manual.html) project.
Denormal numbers are intentionally not supported.

For now, output is in Vivado HLS C++ format, so only Xilinx platform is supported.

## Why NnCore?
* Vendor IP library does not provide a specific core you need
* Core in vendor IP library does not meet the accurarcy requirement you need. NnCore generated cores are faithfully rounded, ULP=1.
* IP library provided core only support standard Half/Single/Double-precision, but you want to explore custom widths (like those in Microsoft's Project [Brainwave](https://www.nextplatform.com/2017/08/24/drilling-microsofts-brainwave-soft-deep-leaning-chip/))

## Prerequisites
NnCore uses [Sollya](http://sollya.gforge.inria.fr/) for minimax-polynomial generations, 
and the fp2bin tool from [FloPoCo](http://flopoco.gforge.inria.fr/) for building ROM contents.

User need to make sure these executables are searchable from their $PATH variable,
by either installing them to system paths, or making symbolic links, e.g.: 
```
ln -s $SOLLYA_PATH/sollya $HOME/bin/sollya
```

## Usage
Example usage script is provided, namely `n2run.sh`.

`run.sh` and `run2.sh` are for the obsolete `nncore.py`.

For more usage info, do `nncore2.py -h`.

List of default supported operators: 

* sigmoid, d'sigmoid, tanh, d'tanh, atan, d'atan

If you want to try NnCore on other operators, 
you'll have to add the corresponding entry in the funcDict around line [911](https://bitbucket.org/hku-casr/nncore/src/3949ebc02dd7b98a36b34f2d25a524136a5f92fb/src/nncore2.py?at=master&fileviewer=file-view-default#nncore2.py-911),
and the command line option in the `main()` function around line [979](https://bitbucket.org/hku-casr/nncore/src/3949ebc02dd7b98a36b34f2d25a524136a5f92fb/src/nncore2.py?at=master&fileviewer=file-view-default#nncore2.py-979) 
and [1017](https://bitbucket.org/hku-casr/nncore/src/3949ebc02dd7b98a36b34f2d25a524136a5f92fb/src/nncore2.py?at=master&fileviewer=file-view-default#nncore2.py-1017).

## How to cite
For the current version (nncore2.py), please cite:
```latex
@INPROCEEDINGS{ho2017nncore,
title={{NnCore}: A Parameterized Non-linear Function Generator for Machine Learning Applications in FPGAs},
author={S. M. H. Ho and H. K. H. So},
booktitle={2017 International Conference on Field-Programmable Technology(FPT)},
year={to appear},
organization={IEEE}
}
```

An obsolete version of NnCore (nncore.py) is also available, for that please cite:
```latex
@INPROCEEDINGS{7966657, 
author={S. M. H. Ho and C. H. D. Hung and H. C. Ng and M. Wang and H. K. H. So}, 
booktitle={2017 IEEE 25th Annual International Symposium on Field-Programmable Custom Computing Machines (FCCM)}, 
title={A Parameterizable Activation Function Generator for FPGA-Based Neural Network Applications}, 
year={2017}, 
volume={}, 
number={}, 
pages={84-84}, 
keywords={field programmable gate arrays;floating point arithmetic;minimax techniques;neural chips;polynomials;search problems;transfer functions;FPGA;NnCore;binary search algorithm;field programmable gate array;minimax-polynomial segments;neural network;open-source parameterized floating-point core generator;parameterizable activation function generator;segments monotonicity;Approximation algorithms;Artificial neural networks;Clocks;Electronic mail;Generators;Table lookup;FPGA;activation function;neural network}, 
doi={10.1109/FCCM.2017.40}, 
ISSN={}, 
month={April},
}
```
