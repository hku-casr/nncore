#!/bin/bash
for e in $(seq 5 -1 1)
#for e in 5
do
  mkdir -p err-${e}
  cd err-${e}
  { 
    #for idx in 3
    for idx in {3..13}
    do
      echo "../nncore.py tanh       --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0       2> err_half_${idx}_tanh.log" 
      echo "../nncore.py sigmoid    --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0       2> err_half_${idx}_sigmoid.log" 
      echo "../nncore.py d_tanh     --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0       2> err_half_${idx}_dtanh.log" 
      echo "../nncore.py d_sigmoid  --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0       2> err_half_${idx}_dsigmoid.log" 
      echo "../nncore.py tanh       --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0       2> err_single_${idx}_tanh.log" 
      echo "../nncore.py sigmoid    --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0       2> err_single_${idx}_sigmoid.log" 
      echo "../nncore.py d_tanh     --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0       2> err_single_${idx}_dtanh.log" 
      echo "../nncore.py d_sigmoid  --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0       2> err_single_${idx}_dsigmoid.log" 
    done
    #for idx in 3
    for idx in 3 5 7 9 11 13
    do
      echo "../nncore.py tanh       --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0 --odd 2> err_half_odd_${idx}_tanh.log"
      echo "../nncore.py sigmoid    --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0 --odd 2> err_half_odd_${idx}_sigmoid.log"
      echo "../nncore.py d_tanh     --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0 --odd 2> err_half_odd_${idx}_dtanh.log"
      echo "../nncore.py d_sigmoid  --wE 5 --wF 10 -d ${idx} --approx_err ${e}.0 --odd 2> err_half_odd_${idx}_dsigmoid.log"
      echo "../nncore.py tanh       --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0 --odd 2> err_single_odd_${idx}_tanh.log"
      echo "../nncore.py sigmoid    --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0 --odd 2> err_single_odd_${idx}_sigmoid.log"
      echo "../nncore.py d_tanh     --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0 --odd 2> err_single_odd_${idx}_dtanh.log"
      echo "../nncore.py d_sigmoid  --wE 8 --wF 23 -d ${idx} --approx_err ${e}.0 --odd 2> err_single_odd_${idx}_dsigmoid.log"
    done 
  } | xargs -t -l -P 32 -I{} timeout 15m bash -c '{}'
  #} | wc -l
  cd ..
done
