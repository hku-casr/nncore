import subprocess
import logging
from nncore2 import Mono

def run(command, log=False):
    # use universal_newlines such that input output are string not bytes in python3
    p = subprocess.Popen(['sollya'], shell=False, stdin=subprocess.PIPE, 
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         universal_newlines=True)
    killed = False
    stdoutdata = ''
    stderrdata = ''
    try:
        # added timeout, in seconds, from python3
        (stdoutdata, stderrdata) = p.communicate(command, timeout=300)
    except subprocess.TimeoutExpired:
        p.kill()
        killed = True
    if log:
        logging.debug(' ------ SOLLYA_LOG ------ ')
        if killed:
            logging.debug(' SOLLYA timeout killed!')
        for s in stdoutdata.split('\n'):
            if s != '':
                logging.debug(' SOLLYA_OUT: %s', s)
        for s in stderrdata.split('\n'):
            if s != '':
                logging.debug(' SOLLYA_ERR: %s', s)
    return stdoutdata

# Following messages from Sollya was suppressed
# Warning (171): the identifier "x" is bound to the current free variable. 
# In a functional context it will be considered as the identity function.
def eval(a, log=False):
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += 'suppressmessage(171);\n'
    sollya_command += a + '\n'
    if log:
        logging.debug(' sollya.eval: ------ SOLLYA_CMD ------ ')
        logging.debug('\n' + sollya_command)
    sollya_out = run(sollya_command, log=log).split('\n')
    return sollya_out[0]

#def transform_func(spec, x_lo, x_up, x_e, y_e, x_sgn, y_sgn):
def transform_func(spec, seg):
    x_lo = seg.x_lo
    x_up = seg.x_up
    x_e = seg.x_e
    y_e = seg.y_e
    x_sgn = seg.x_sgn
    y_sgn = seg.y_sgn
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += spec.transDict['x_u2f'] % (x_sgn, x_e)
    #sollya_command += "x_u2f = (-1)^(%s)*(2^(%s))*(1+x);\n" % (x_sgn, x_e)
    #sollya_command += "f = %s;\n" % spec.fdef
    sollya_command += spec.fdef + '\n'
    sollya_command += spec.transDict['y_f2u'] % (y_sgn, y_e)
    #sollya_command += "y_f2u = (-1)^(%s)*(1/2^(%s))*x - 1;\n" % (y_sgn, y_e)
    sollya_command += 'y_f2u(f(x_u2f));\n'
    #sollya_command += 'tr(f);\n'
    sollya_out = run(sollya_command, log=False).split('\n')
    return sollya_out[0]

def divide_seg(spec, x_lo, x_up, x_e, x_sgn, divider):
    cond = eval("ulp = 0x1p{}; ({} - {} > ulp*{});"
            .format(int(x_e) - spec.wf, x_up, x_lo, divider))
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += "ulp = 0x1p%d;\n" % (int(x_e) - spec.wf)
    sollya_command += "lo = %s;\n" % x_lo
    sollya_command += "up = %s;\n" % x_up
    sollya_command += "div = %d;\n" % divider 
    sollya_command += "dif = up - lo;\n"
    if cond == 'true':
        #sollya_command += 'if (dif > ulp*div) then {\n'
        sollya_command += '  d = dif/div;\n'
        sollya_command += "  for i from 1 to %d do {\n" % (divider-1)
        sollya_command += "    new_lo = round(%s + i*d, %s, RN);\n" % (x_lo, spec.wf+1)
        sollya_command += '    new_up = new_lo - ulp;\n'
        sollya_command += '    new_up;\n'
        sollya_command += '    new_lo;\n'
        sollya_command += '  };\n'
    else:
        #sollya_command += '} else {\n'
        sollya_command += '  while (lo <= up) do {\n'
        sollya_command += '    lo;\n'
        sollya_command += '    lo = lo + ulp;\n'
        sollya_command += '  };\n'
        #sollya_command += '};\n'
    logging.debug(' sollya.divide_seg: ------ SOLLYA_CMD ------ ')
    logging.debug('\n' + sollya_command)
    sollya_out = run(sollya_command, log=True).split('\n')
    subsegs = list()
    if cond == 'true':
        subsegs.append((x_lo, sollya_out[0]))
        if divider > 2:
            for i in range(1, divider-1):
                subsegs.append((sollya_out[(i-1)*2+1], sollya_out[i*2]))
        subsegs.append((sollya_out[(divider-2)*2+1], x_up))
    else:
        for i in sollya_out:
            if i:
                subsegs.append((i, i))
    return (subsegs, cond=='true')

def approx(spec, fstr, seg):
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += ('suppressmessage(127, 128, 171, 189, 282, 285, 301, 307, '
                       '345, 346, 355, 369, 379, 401, 402);\n')
    # increasing from default 165 to makes supnorm work properly
    sollya_command += 'prec = 1000!;\n' 
    sollya_command += "deg = %d;\n" % spec.deg
    sollya_command += "fs = %s;\n" % fstr
    sollya_command += spec.transDict['x_f2u'] % (seg.x_sgn, seg.x_e)
    #sollya_command += "x_f2u = (-1)^(%s)*(1/2^(%s))*x - 1;\n" % (seg.x_sgn, seg.x_e)
    if seg.x_sgn == '0':
        sollya_command += "lo = %s; up = %s;\n" % (seg.x_lo, seg.x_up)
    else:
        sollya_command += "lo = %s; up = %s;\n" % (seg.x_up, seg.x_lo)
    sollya_command += 'r = [x_f2u(lo);x_f2u(up)];\n'
    sollya_command += 'p = remez(fs, deg, r);\n'
    sollya_command += 'err = dirtyinfnorm(fs - p, r);\n'
    sollya_command += 'p;\n'
    sollya_command += 'round(err, 24, RN);\n'
    sollya_out = run(sollya_command, log=True)
    if sollya_out:
        out = sollya_out.split('\n')
        return (out[0], out[1])
    else:
        return ('', 'infty')

def fixapprox2(spec, seg, deg):
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += ('suppressmessage(127, 128, 171, 189, 282, 285, 301, 307, '
                       '345, 346, 355, 369, 379, 401, 402);\n')
    # increasing from default 165 to makes supnorm work properly
    sollya_command += 'prec = 1000!;\n' 

    # merged const seg or single points
    if ((not seg.x_e) or (seg.x_lo == seg.x_up)):
        sollya_command += "y := %s;\n" % seg.x_lo_y
        sollya_command += 'match y with\n'
        sollya_command += ' NaN : (0)\n'
        sollya_command += ' 0 : (0)\n'
        sollya_command += ' -infty : (0)\n'
        sollya_command += ' infty : (0)\n'
        sollya_command += ' default : {\n'
        sollya_command += '     var p;\n'
        sollya_command += "     " + spec.transDict['y_f2u'] % (seg.y_sgn, seg.y_e)
        #sollya_command += "     y_f2u = (-1)^(%s)*(1/2^(%s))*x - 1;\n" % (seg.y_sgn, seg.y_e)
        sollya_command += '     p = y_f2u(y);'
        sollya_command += '     return p;'
        sollya_command += ' };\n'
        sollya_command += '0;\n'
    else:
        sollya_command += "fs = %s;\n" % seg.f_str
        if seg.p_scale:
            sollya_command += "fscal = 2^(%d)*%s;\n" % (-seg.p_scale, seg.f_str)
        sollya_command += "deg = %d;\n" % deg
        sollya_command += 'formats = [||];\n'
        sollya_command += "for i from 0 to deg do formats[i] = %s - i;\n" % (spec.wf+1)
        sollya_command += spec.transDict['x_f2u'] % (seg.x_sgn, seg.x_e)
        #sollya_command += "x_f2u = (-1)^(%s)*(1/0x1p%s)*x - 1;\n" % (seg.x_sgn, seg.x_e)
        if seg.x_sgn == '0':
            sollya_command += "lo = %s; up = %s;\n" % (seg.x_lo, seg.x_up)
        else:
            sollya_command += "lo = %s; up = %s;\n" % (seg.x_up, seg.x_lo)
        sollya_command += 'r = [x_f2u(lo);x_f2u(up)];\n'
        if seg.p_scale:
            sollya_command += 'p = fpminimax(fscal, deg, formats, r, fixed);\n'
            sollya_command += 'err = sup(supnorm(p*2^(%d), fs, r, absolute, 1b-40));\n' % seg.p_scale
        else:
            sollya_command += 'p = fpminimax(fs, deg, formats, r, fixed);\n'
            sollya_command += 'err = sup(supnorm(p, fs, r, absolute, 1b-40));\n'
        sollya_command += 'p;\n'
        sollya_command += 'round(err, 24, RN);\n'
    #logging.debug(' sollya.fixapprox2: ------ SOLLYA_CMD ------ ')
    #logging.debug('\n' + sollya_command)
    sollya_out = run(sollya_command, log=True)
    if sollya_out:
        out = sollya_out.split('\n')
        return (out[0], out[1])
    else:
        return ('', 'infty')

# Following messages from Sollya was suppressed
# Warning (127): the given expression or command could not be handled.
# Warning (128): at least one of the given expressions or a subexpression is not correctly typed
#                or its evaluation has failed because of some error on a side-effect.
# Warning (171): the identifier "x" is bound to the current free variable. 
#                In a functional context it will be considered as the identity function.
# Warning (189): the supremum norm on the error function between the given polynomial and 
#                the given function could not be computed.
# Warning (282): during supnorm computation, no suitable Taylor form could be found.
# Warning (285): during supnorm computation, the positivity of a polynomial could not be established.
# Warning (301): an error occurred during supremum norm computation. 
#                A safe enclosure of the supremum norm could not be computed.
# Warning (307): the given polynomial is the zero polynomial. Its number of zeros is infinite.
# Warning (345) in Remez: main heuristic failed. A slower algorithm is used for this step.
# Warning (346) in Remez: a slower algorithm is used for this step
# Warning (355): degenerated system in a non Haar context. The algorithm may be incorrect.
# Error (369) in Remez: the algorithm does not converge.
# Warning (379): fpminimax failed to recover the coefficients from the minimax pseudo-polynomial
# Warning (401): the expression is constant.
# Warning (402): the evaluation of the given function ... in ... gives NaN.
#                This (possibly maximum) point will be excluded from the infnorm result.
#def fixapprox(spec, f_s, x_lo, x_up, x_e, x_sgn, y_e, y_sgn, d):
def fixapprox(spec, seg, f_s, d):
    #(x_lo, x_up, x_e, y_e, x_sgn, y_sgn) = seg
    x_lo = seg.x_lo 
    x_up = seg.x_up 
    x_e = seg.x_e 
    y_e = seg.y_e 
    x_sgn = seg.x_sgn 
    y_sgn = seg.y_sgn 
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += ('suppressmessage(127, 128, 171, 189, 282, 285, 301, 307, '
                       '345, 346, 355, 369, 379, 401, 402);\n')
    # increasing from default 165 to makes supnorm work properly
    sollya_command += 'prec = 1000!;\n' 
    sollya_command += "deg = %d;\n" % d
    sollya_command += "fs = %s;\n" % f_s
    sollya_command += 'formats = [|0|];\n'
    # WF+1 bit after the point for c0
    sollya_command += "for i from 0 to deg do formats[i] = %s - i;\n" % (spec.wf+1)
    sollya_command += 'formats;\n' # out0
    sollya_command += "x_f2u = (-1)^(%s)*(1/0x1p%s)*x - 1;\n" % (x_sgn, x_e)
    if x_sgn == '0':
        #sollya_command += "r = [x_f2u(%s);x_f2u(%s)];\n" % (x_lo, x_up)
        sollya_command += "lo = %s;\n" % x_lo
        sollya_command += "up = %s;\n" % x_up
    else:
        #sollya_command += "r = [x_f2u(%s);x_f2u(%s)];\n" % (x_up, x_lo)
        sollya_command += "lo = %s;\n" % x_up
        sollya_command += "up = %s;\n" % x_lo
    sollya_command += 'r = [x_f2u(lo);x_f2u(up)];\n'
    sollya_command += 'r;\n' # out1
    sollya_command += 'p = fpminimax(fs, deg, formats, r, fixed);\n'
    #sollya_command += 'p = fpminimax(f, deg, formats, [0;1], fixed);\n' # experimental!!!
    sollya_command += 'p;\n' # out2
    sollya_command += 'err = sup(supnorm(p, fs, r, absolute, 1b-40));\n'
    sollya_command += 'round(err, 24, RN);\n' # out3
    sollya_command += "y_u2f = (-1)^(%s)*(0x1p%s)*(1+x);\n" % (y_sgn, y_e)
    sollya_command += spec.fdef + '\n'
    sollya_command += "err2 = sup(supnorm(y_u2f(p(x_f2u)), f, [%s;%s], absolute, 1b-40));\n" % (x_lo, x_up)
    sollya_command += 'round(err2, 24, RN);\n' # out4
    #logging.debug(' sollya.fix_approx: ------ SOLLYA_CMD ------ ')
    #logging.debug('\n' + sollya_command)
    sollya_out = run(sollya_command, log=True)
    if not sollya_out:
        return ('', '', '')
    else:
        out = sollya_out.split('\n')
        return (out[2], out[3], out[4])


def compare(cmpstr):
    sollya_command = 'showmessagenumbers = on!;\n'
    #sollya_command += 'suppressmessage(162);\n'
    sollya_command += cmpstr
    sollya_out = run(sollya_command).split('\n')
    if sollya_out[0] == 'true': 
        return True
    else: 
        return False


def equal(a, b):
    return compare("%s == %s;\n" % (a, b))

def lessOrEqual(a, b):
    return compare("%s <= %s;\n" % (a, b))

def less(a, b):
    return compare("%s < %s;\n" % (a, b))

def greaterOrEqual(a, b):
    return compare("%s >= %s;\n" % (a, b))

def greater(a, b):
    return compare("%s > %s;\n" % (a, b))


def yrnd(spec, x):
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += 'suppressmessage(171);\n'
    sollya_command += spec.fdef + '\n'
    sollya_command += 'y := f(%s);\n' % x
    sollya_command += 'match y with\n'
    sollya_command += ' NaN : (NaN)\n'
    sollya_command += ' 0 : (0)\n'
    sollya_command += ' -infty : (-infty)\n'
    sollya_command += ' infty : (infty)\n'
    sollya_command += ' default : {\n'
    sollya_command += '     var yr, ye, ret;\n'
    sollya_command += "     yr = round(y, %s, RN);\n" % (spec.wf+1)
    sollya_command += '     ye = floor(log2(abs(yr)));\n' 
    sollya_command += "     if (ye < %s) then ret = 0 \n" % spec.exp_lo
    sollya_command += "     else if (ye > %s) then ret = %s\n" % (spec.exp_up, spec.y_up)
    sollya_command += '     else ret = yr;\n'
    sollya_command += '     return ret;\n'
    sollya_command += ' };\n'
    #logging.debug(' sollya.yrnd: ------ SOLLYA_CMD ------ ')
    #logging.debug('\n' + sollya_command)
    sollya_out = run(sollya_command, log=False).split('\n')
    return sollya_out[0]


def bin(x):
    sollya_command = 'display = decimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += 'suppressmessage(171);\n'
    sollya_command += "y := %s;\n" % x
    sollya_command += 'match y with\n'
    sollya_command += ' NaN : (NaN)\n'
    sollya_command += ' 0 : (0)\n'
    sollya_command += ' -infty : (-infty)\n'
    sollya_command += ' infty : (infty)\n'
    sollya_command += ' default : {\n'
    sollya_command += '     return floor(log2(abs(y)));\n' 
    sollya_command += ' };\n'
    sollya_command += 'if (y<0) then 1 else 0;\n'
    sollya_out = run(sollya_command, log=False).split('\n')
    return (sollya_out[1], sollya_out[0])


def bin_eq(a, b):
    return bin(a)==bin(b)

def midpoint(spec, a, b):
    eval_str = "round((%s + %s)/2, %s, RD);" % (a, b, spec.wf+1)
    return eval(eval_str)

def add_ulp(spec, x):
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += 'suppressmessage(171);\n'
    sollya_command += "y := %s;\n" % x
    sollya_command += 'ye = floor(log2(abs(y)));\n' 
    sollya_command += "ue = ye - %s;\n" % spec.wf
    sollya_command += 'y + 2^(ue);\n'
    sollya_out = run(sollya_command, log=False).split('\n')
    return sollya_out[0]


def x_of_limit(spec, xinc=True, limit=None, log=False):
    sollya_command = 'display = hexadecimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += spec.fdef + '\n'
    sollya_command += "invf = %s;\n" % spec.invf
    sollya_command += "n = %d;\n" % (spec.wf + 1)
    if limit is None:
        if ((spec.mono == Mono.INC and xinc) or 
                (spec.mono == Mono.DEC and not xinc)):
            sollya_command += "r_ceil = %s;\n" % spec.y_max
        else:
            sollya_command += "r_ceil = %s;\n" % spec.y_min
    else:
        sollya_command += "r_ceil = %s;\n" % limit

    if xinc:
        sollya_command += 'd_init = round(invf(r_ceil), n, RD);\n'
    else:
        sollya_command += 'd_init = round(invf(r_ceil), n, RU);\n'

    sollya_command += 'd_ceil = d_init;\n'
    sollya_command += 'if (d_init == 0) then {\n'
    sollya_command += '    d_exp = 0;\n'
    sollya_command += "    loop = %d;\n" % (spec.wf + 2**(spec.we-1) - 1)
    sollya_command += '} else {\n'
    # !!!Caution!!! exponent in Sollya gives a dyadic form m.2^e where m is odd integer, e integer
    #sollya_command += '    d_exp = exponent(d_init);\n' 
    sollya_command += '    d_exp = floor(log2(abs(d_init)));\n'
    sollya_command += '    loop = (n);\n' # originally n-1
    sollya_command += '};\n'
    sollya_command += 'for d from 1 to loop do {\n'

    if xinc:
        sollya_command += '  guess = d_ceil + 2^(d_exp - d);\n'
    else:
        sollya_command += '  guess = d_ceil - 2^(d_exp - d);\n'

    sollya_command += '  test = round(f(round(guess, n, RN)), n, RN);\n'

    if ((spec.mono == Mono.INC and xinc) or 
            (spec.mono == Mono.DEC and not xinc)):
        sollya_command += '  if (test <= r_ceil) then \n'
    else:
        sollya_command += '  if (test >= r_ceil) then \n'

    sollya_command += '     d_ceil = guess;\n'
    sollya_command += '};\n'
    sollya_command += 'round(d_ceil, n, RN);\n'
    #sollya_command += 'if (d_init == 0) then round(d_ceil, n, RN)\n'
    #sollya_command += 'else d_ceil;\n'
    if log:
        logging.debug(' ------ sollya.x_of_limit SOLLYA_CMD ------ ')
        logging.debug('\n' + sollya_command)
    return run(sollya_command, log=log).split('\n')[0]
    #return run(sollya_command).split('\n')[0]


