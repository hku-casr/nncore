#!/usr/bin/python
from __future__ import print_function
from __future__ import division
import argparse
import math
import os
import re
import subprocess
import sys
from timeit import default_timer as timer
__author__ = 'sammhho'

#class Symmetry(Enum):
#    NO = 0
#    ODD = 1
#    EVEN = 2

def enum(**enums):
    return type('Enum', (), enums)

Symmetry = enum(NO=0, ODD=1, EVEN=2)
Dir = enum(SHRINK=0, EXPAND=1)
Mono = enum(INC=0, DEC=1)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def run_fp2bin(command):
    cmd_list = ['fp2bin']
    cmd_list.extend(command)
    #print (cmd_list)
    fp2bin = subprocess.Popen(cmd_list, shell=False, stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return fp2bin.communicate()[0]

def run_flopoco(command):
    cmd_list = ['flopoco']
    cmd_list.extend(command)
    #print (cmd_list)
    flopoco = subprocess.Popen(cmd_list, shell=False, stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return flopoco.communicate()[0]

def run_sollya(command):
    sollya = subprocess.Popen(['sollya','--warnonstderr'], shell=True, stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return sollya.communicate(command)[0]

def print_header_cmp_cndt_code(wE, wF):
    line  = 'module fp_cmp_cndt_code #(\n'
    line += "  parameter WE = %d,\n" % wE
    line += "  parameter WF = %d\n" % wF
    line += ')(\n'
    line += '  input aclk,\n'
    line += '  input aresetn,\n'
    line += '  input [WE+WF+2:0] A_TDATA,\n'
    line += '  input             A_TVALID,\n'
    line += '  input [WE+WF+2:0] B_TDATA,\n'
    line += '  input             B_TVALID,\n'
    line += '  output [3:0]      R_TDATA,\n'
    line += '  output            R_TVALID\n'
    line += ');\n'
    line += '\n'
    return line

def print_header_cmp_op(wE, wF, name, pipe):
    line  = "module %s_p%d #(\n" % (name, pipe)
    line += "  parameter WE = %d,\n" % wE
    line += "  parameter WF = %d\n" % wF
    line += ')(\n'
    line += '  input aclk,\n'
    line += '  input aresetn,\n'
    line += '  input [WE+WF+2:0] A_TDATA,\n'
    line += '  input             A_TVALID,\n'
    line += '  input [WE+WF+2:0] B_TDATA,\n'
    line += '  input             B_TVALID,\n'
    line += '  output            R_TDATA,\n'
    line += '  output            R_TVALID\n'
    line += ');\n'
    line += '\n'
    return line

def print_header_in1out1(wE, wF, name):
    line  = "module %s #(\n" % name
    line += "  parameter WE = %d,\n" % wE
    line += "  parameter WF = %d\n" % wF
    line += ')(\n'
    line += '  input aclk,\n'
    line += '  input aresetn,\n'
    line += '  input [WE+WF+2:0] A_TDATA,\n'
    line += '  input             A_TVALID,\n'
    line += '  output [WE+WF+2:0] R_TDATA,\n'
    line += '  output             R_TVALID\n'
    line += ');\n'
    line += '\n'
    return line

def print_header_rom(wE, wF, name, addr_width):
    line  = "module %s (\n" % name
    #line += '(\n'
    line += '  input aclk,\n'
    line += "  input [%d:0] addr,\n" % (addr_width - 1)
    line += "  output reg [%d:0] data\n" % (2 + wE + wF)
    line += ');\n'
    line += '\n'
    return line

def print_wire_cmp_common():
    line  = '// common\n'
    line += 'wire [1:0]    excpt_a;\n'
    line += 'wire          sign_a;\n'
    line += 'wire [WE-1:0] expo_a;\n'
    line += 'wire [WF-1:0] manti_a;\n'
    line += 'wire [1:0]    excpt_b;\n'
    line += 'wire          sign_b;\n'
    line += 'wire [WE-1:0] expo_b;\n'
    line += 'wire [WF-1:0] manti_b;\n'
    line += 'wire both_zero;\n'
    line += 'wire both_norm;\n'
    line += 'reg  both_zero_d1;\n'
    line += 'reg  both_norm_d1;\n'
    line += 'wire out_vld;\n'
    line += 'reg  out_vld_d1;\n'
    line += '\n'
    return line

def print_wire_cmp_eq():
    line  = '// equal\n'
    line += 'wire sign_eq;\n'
    line += 'wire value_eq;\n'
    line += 'wire both_inf;\n'
    line += 'reg  sign_eq_d1;\n'
    line += 'reg  value_eq_d1;\n'
    line += 'reg  both_inf_d1;\n'
    line += 'wire r_eq;\n'
    line += '\n'
    return line

def print_wire_cmp_gtrlss():
    line  = '// grtr/less common\n'
    line += 'wire r_nan;\n'
    line += 'wire expo_eq;\n'
    line += 'wire isPos;\n'
    line += 'wire isNeg;\n'
    line += 'wire excpt_grtr;\n'
    line += 'wire expo_grtr;\n'
    line += 'wire manti_grtr;\n'
    line += 'wire sign_grtr;\n'
    line += 'wire excpt_less;\n'
    line += 'wire expo_less;\n'
    line += 'wire manti_less;\n'
    line += 'wire sign_less;\n'
    line += 'reg  r_nan_d1;\n'
    line += 'reg  expo_eq_d1;\n'
    line += 'reg  isPos_d1;\n'
    line += 'reg  isNeg_d1;\n'
    line += 'reg  excpt_grtr_d1;\n'
    line += 'reg  expo_grtr_d1;\n'
    line += 'reg  manti_grtr_d1;\n'
    line += 'reg  sign_grtr_d1;\n'
    line += 'reg  excpt_less_d1;\n'
    line += 'reg  expo_less_d1;\n'
    line += 'reg  manti_less_d1;\n'
    line += 'reg  sign_less_d1;\n'
    line += 'wire r_grtr;\n'
    line += 'wire r_less;\n'
    line += '\n'
    return line

def print_assign_cmp_common(pipe):
    line  = '// common\n'
    line += 'assign {excpt_a, sign_a, expo_a, manti_a} = A_TDATA;\n'
    line += 'assign {excpt_b, sign_b, expo_b, manti_b} = B_TDATA;\n'
    line += 'assign both_zero = (excpt_a == 2\'b00 && excpt_b == 2\'b00);\n'
    line += 'assign both_norm = (excpt_a == 2\'b01 && excpt_b == 2\'b01);\n'
    line += '\n'
    if pipe == 0:
        line += 'always @(*) begin\n'
        line += '  both_zero_d1 = both_zero;\n'
        line += '  both_norm_d1 = both_norm;\n'
        line += 'end\n'
    else: #elif pipe == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '  if (~aresetn) begin\n'
        line += '    both_zero_d1 <= 1\'b0;\n'
        line += '    both_norm_d1 <= 1\'b0;\n'
        line += '  end\n'
        line += '  else begin\n'
        line += '    both_zero_d1 <= both_zero;\n'
        line += '    both_norm_d1 <= both_norm;\n'
        line += '  end\n'
        line += 'end\n'
    line += '\n'
    return line

def print_assign_cmp_eq(pipe):
    line  = '// equal\n'
    line += 'assign sign_eq = (sign_a == sign_b);\n'
    line += 'assign value_eq = \
            ({sign_a, expo_a, manti_a} == {sign_b, expo_b, manti_b});\n'
    line += 'assign both_inf = (excpt_a == 2\'b10 && excpt_b == 2\'b10);\n'
    line += '\n'
    if pipe == 0:
        line += 'always @(*) begin\n'
        line += '  sign_eq_d1 = sign_eq;\n'
        line += '  value_eq_d1 = value_eq;\n'
        line += '  both_inf_d1 = both_inf;\n'
        line += 'end\n'
    else: #elif pipe == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '  if (~aresetn) begin\n'
        line += '    sign_eq_d1 <= 1\'b0;\n'
        line += '    value_eq_d1 <= 1\'b0;\n'
        line += '    both_inf_d1 <= 1\'b0;\n'
        line += '  end\n'
        line += '  else begin\n'
        line += '    sign_eq_d1 <= sign_eq;\n'
        line += '    value_eq_d1 <= value_eq;\n'
        line += '    both_inf_d1 <= both_inf;\n'
        line += '  end\n'
        line += 'end\n'
    line += '\n'
    line += 'assign r_eq = both_zero_d1 || (both_inf_d1 && sign_eq_d1) || ' \
            '(both_norm_d1 && value_eq_d1);\n'
    line += '\n'
    return line

def print_assign_cmp_gtrlss(pipe):
    line  = '// grtr/less common\n'
    line += 'assign r_nan = (excpt_a == 2\'b11 || excpt_b == 2\'b11);\n'
    line += 'assign expo_eq = (expo_a == expo_b);\n'
    line += 'assign isPos = (sign_a == 1\'b0 && sign_b == 1\'b0);\n'
    line += 'assign isNeg = (sign_a == 1\'b1 && sign_b == 1\'b1);\n'
    line += 'assign excpt_grtr = (excpt_a > excpt_b);\n'
    line += 'assign expo_grtr = (expo_a > expo_b);\n'
    line += 'assign manti_grtr = (manti_a > manti_b);\n'
    line += 'assign excpt_less = (excpt_a < excpt_b);\n'
    line += 'assign expo_less = (expo_a < expo_b);\n'
    line += 'assign manti_less = (manti_a < manti_b);\n'
    line += 'assign sign_grtr = (sign_a == 1\'b0 && sign_b == 1\'b1);\n'
    line += 'assign sign_less = (sign_a == 1\'b1 && sign_b == 1\'b0);\n'
    line += '\n'
    if pipe == 0:
        line += 'always @(*) begin\n'
        line += '  r_nan_d1 = r_nan;\n'
        line += '  expo_eq_d1 = expo_eq;\n'
        line += '  isPos_d1 = isPos;\n'
        line += '  isNeg_d1 = isNeg;\n'
        line += '  excpt_grtr_d1 = excpt_grtr;\n'
        line += '  expo_grtr_d1 = expo_grtr;\n'
        line += '  manti_grtr_d1 = manti_grtr;\n'
        line += '  excpt_less_d1 = excpt_less;\n'
        line += '  expo_less_d1 = expo_less;\n'
        line += '  manti_less_d1 = manti_less;\n'
        line += '  sign_grtr_d1 = sign_grtr;\n'
        line += '  sign_less_d1 = sign_less;\n'
        line += 'end\n'
    else: #elif pipe == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '  if (~aresetn) begin\n'
        line += '    r_nan_d1 <= 1\'b0;\n'
        line += '    expo_eq_d1 <= 1\'b0;\n'
        line += '    isPos_d1 <= 1\'b0;\n'
        line += '    isNeg_d1 <= 1\'b0;\n'
        line += '    excpt_grtr_d1 <= 1\'b0;\n'
        line += '    expo_grtr_d1 <= 1\'b0;\n'
        line += '    manti_grtr_d1 <= 1\'b0;\n'
        line += '    excpt_less_d1 <= 1\'b0;\n'
        line += '    expo_less_d1 <= 1\'b0;\n'
        line += '    manti_less_d1 <= 1\'b0;\n'
        line += '    sign_grtr_d1 <= 1\'b0;\n'
        line += '    sign_less_d1 <= 1\'b0;\n'
        line += '  end\n'
        line += '  else begin\n'
        line += '    r_nan_d1 <= r_nan;\n'
        line += '    expo_eq_d1 <= expo_eq;\n'
        line += '    isPos_d1 <= isPos;\n'
        line += '    isNeg_d1 <= isNeg;\n'
        line += '    excpt_grtr_d1 <= excpt_grtr;\n'
        line += '    expo_grtr_d1 <= expo_grtr;\n'
        line += '    manti_grtr_d1 <= manti_grtr;\n'
        line += '    excpt_less_d1 <= excpt_less;\n'
        line += '    expo_less_d1 <= expo_less;\n'
        line += '    manti_less_d1 <= manti_less;\n'
        line += '    sign_grtr_d1 <= sign_grtr;\n'
        line += '    sign_less_d1 <= sign_less;\n'
        line += '  end\n'
        line += 'end\n'
    line += '\n'
    return line

def print_assign_cmp_grtr():
    line  = '// grtr\n'
    line += 'assign r_grtr = (!r_nan_d1 && !both_zero_d1 && (sign_grtr_d1 || \n'
    line += '  (isPos_d1 && (excpt_grtr_d1 || \n'
    line += '  (both_norm_d1 && (expo_grtr_d1 || (expo_eq_d1 && manti_grtr_d1))))) || \n'
    line += '  (isNeg_d1 && (excpt_less_d1 ||\n'
    line += '  (both_norm_d1 && (expo_less_d1 || (expo_eq_d1 && manti_less_d1))))) \n'
    line += '));\n'
    line += '\n'
    return line

def print_assign_cmp_less():
    line  = '// less\n'
    line += 'assign r_less = (!r_nan_d1 && !both_zero_d1 && (sign_less_d1 || \n'
    line += '  (isPos_d1 && (excpt_less_d1 || \n'
    line += '  (both_norm_d1 && (expo_less_d1 || (expo_eq_d1 && manti_less_d1))))) || \n'
    line += '  (isNeg_d1 && (excpt_grtr_d1 ||\n'
    line += '  (both_norm_d1 && (expo_grtr_d1 || (expo_eq_d1 && manti_grtr_d1))))) \n'
    line += '));\n'
    line += '\n'
    return line

def print_assign_cmp_cndt_code(pipe):
    line  = '// cndt code\n'
    line += 'assign out_vld = A_TVALID & B_TVALID;\n'
    if pipe == 0:
        line += 'assign R_TDATA = {r_nan, r_grtr, r_less, r_eq};\n\n'
        line += 'always @(*) begin\n'
        line += '  out_vld_d1 = out_vld;\n'
        line += 'end\n'
    else: #elif pipe == 1:
        line += 'assign R_TDATA = {r_nan_d1, r_grtr, r_less, r_eq};\n\n'
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '  if (~aresetn) begin\n'
        line += '    out_vld_d1 <= 1\'b0;\n'
        line += '  end\n'
        line += '  else begin\n'
        line += '    out_vld_d1 <= out_vld;\n'
        line += '  end\n'
        line += 'end\n'
    line += '\n'
    line += 'assign R_TVALID = out_vld_d1;\n'
    return line

def print_assign_cmp_op(expr, pipe):
    line  = '// less\n'
    line += 'assign out_vld = A_TVALID & B_TVALID;\n'
    line += "assign R_TDATA = %s;\n\n" % expr
    if pipe == 0:
        line += 'always @(*) begin\n'
        line += '  out_vld_d1 = out_vld;\n'
        line += 'end\n'
    else: #elif pipe == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '  if (~aresetn) begin\n'
        line += '    out_vld_d1 <= 1\'b0;\n'
        line += '  end\n'
        line += '  else begin\n'
        line += '    out_vld_d1 <= out_vld;\n'
        line += '  end\n'
        line += 'end\n'
    line += '\n'
    line += 'assign R_TVALID = out_vld_d1;\n'
    return line

def print_wire_in1():
    line  = '// Common for 1 input\n'
    line += 'wire [1:0]    excpt_a;\n'
    line += 'wire          sign_a;\n'
    line += 'wire [WE-1:0] expo_a;\n'
    line += 'wire [WF-1:0] manti_a;\n'
    line += '\n'
    line += 'assign {excpt_a, sign_a, expo_a, manti_a} = A_TDATA;\n'
    line += '\n'
    return line

def print_wire_in16():
    line  = '// ReLU6\n'
    line += 'wire [WE+WF+2:0] relu_out;\n'
    line += 'wire             is_less;\n'
    line += 'reg  [1:0]       excpt_a_d1;\n'
    return line

def print_assign_relu():
    line  = '// ReLU\n'
    line += 'assign R_TDATA = (sign_a == 1\'b1 && excpt_a != 2\'b11)? ' \
            '\'b0 : A_TDATA;\n'
    line += 'assign R_TVALID = A_TVALID;\n'
    line += '\n'
    return line

def print_assign_relu6(wE, wF, pipe, const):
    line  = '// ReLU6\n'
    line += 'assign relu_out = (sign_a == 1\'b1 && excpt_a != 2\'b11)? ' \
            '\'b0 : A_TDATA;\n'
    line += '\n'
    line += "fp_cmp_lss_p%d cmp_lss_inst\n" % pipe
    line += '(\n'
    line += '  .aclk        (aclk),\n'
    line += '  .aresetn     (aresetn),\n'
    line += '  .A_TDATA     (relu_out),\n'
    line += '  .A_TVALID    (A_TVALID),\n'
    line += "  .B_TDATA     (%s),\n" % ("%d\'b" % (3 + wE + wF) + const)
    line += '  .B_TVALID    (1\'b1),\n'
    line += '  .R_TDATA     (is_less),\n'
    line += '  .R_TVALID    (out_vld)\n'
    line += ');\n'
    line += '\n'
    if pipe == 0:
        line += "assign R_TDATA = (is_less || excpt_a == 2\'b11)? relu_out : " \
                "%s;\n" % ("%d\'b" % (3 + wE + wF) + const)
        line += 'assign R_TVALID = A_TVALID;'
    else: #elif pipe == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '  if (~aresetn) begin\n'
        line += '    relu_out_d1 <= \'b0;\n'
        line += '    excpt_a_d1 <= \'b0;\n'
        line += '  end\n'
        line += '  else begin\n'
        line += '    relu_out_d1 <= relu_out;\n'
        line += '    excpt_a_d1 <= excpt_a;\n'
        line += '  end\n'
        line += 'end\n'
        line += '\n'
        line += "assign R_TDATA = (is_less || excpt_a_d1 == 2\'b11)? relu_out_d1 : " \
                "%s;\n" % ("%d\'b" % (3 + wE + wF) + const)
        line += 'assign R_TVALID = out_vld;\n'
        line += '\n'
    return line

def print_footer(name):
    line = 'endmodule\n'
    line += "// %s\n" % name
    return line

def gen_cmp_cndt_code(wE, wF, pipe):
    line = ''
    line += print_header_cmp_cndt_code(wE, wF)
    line += print_wire_cmp_common()
    line += print_wire_cmp_eq()
    line += print_wire_cmp_gtrlss()
    line += print_assign_cmp_common(pipe)
    line += print_assign_cmp_eq(pipe)
    line += print_assign_cmp_gtrlss(pipe)
    line += print_assign_cmp_grtr()
    line += print_assign_cmp_less()
    line += print_assign_cmp_cndt_code(pipe)
    line += print_footer('fp_cmp_cndt_code')
    file_out = open("./fp_cmp_cndt_code_%d_%d_p%d.v" % (wE, wF, pipe), 'w')
    file_out.write(line);
    file_out.close()

def gen_cmp_lss(wE, wF, pipe):
    line = ''
    line += print_header_cmp_op(wE, wF, 'fp_cmp_lss', pipe)
    line += print_wire_cmp_common()
    line += print_wire_cmp_gtrlss()
    line += print_assign_cmp_common(pipe)
    line += print_assign_cmp_gtrlss(pipe)
    line += print_assign_cmp_less()
    line += print_assign_cmp_op('r_less', pipe)
    line += print_footer('fp_cmp_lss')
    file_out = open("./fp_cmp_lss_%d_%d_p%d.v" % (wE, wF, pipe), 'w')
    file_out.write(line);
    file_out.close()

def gen_cmp_leq(wE, wF, pipe):
    line = ''
    line += print_header_cmp_op(wE, wF, 'fp_cmp_leq', pipe)
    line += print_wire_cmp_common()
    line += print_wire_cmp_eq()
    line += print_wire_cmp_gtrlss()
    line += print_assign_cmp_common(pipe)
    line += print_assign_cmp_eq(pipe)
    line += print_assign_cmp_gtrlss(pipe)
    line += print_assign_cmp_less()
    line += print_assign_cmp_op('r_less | r_eq', pipe)
    line += print_footer('fp_cmp_leq')
    file_out = open("./fp_cmp_leq_%d_%d_p%d.v" % (wE, wF, pipe), 'w')
    file_out.write(line);
    file_out.close()

def gen_relu(wE, wF):
    line = ''
    line += print_header_in1out1(wE, wF, 'fp_relu')
    line += print_wire_in1()
    line += print_assign_relu()
    line += print_footer('fp_relu')
    file_out = open("./fp_relu_%d_%d.v" % (wE, wF), 'w')
    file_out.write(line);
    file_out.close()

def gen_relu6(wE, wF, pipe, const):
    print('In ReLU6, const is ' + str(const))
    fp2bin_out = run_fp2bin([str(wE), str(wF), str(const)]).split('\n')[0]
    print("fp2bin_out is %s" % fp2bin_out)
    gen_cmp_lss(wE, wF, pipe)
    line = ''
    line += print_header_in1out1(wE, wF, 'fp_relu6')
    line += print_wire_in16()
    line += print_wire_in1()
    line += print_assign_relu6(wE, wF, pipe, fp2bin_out)
    line += print_footer('fp_relu6')
    file_out = open("./fp_relu6_%d_%d_c%s.v" % (wE, wF, str(const)), 'w')
    file_out.write(line);
    file_out.close()

#def prev_double(x):
#    sollya_command = "display = hexadecimal!;"
#    sollya_command += "round(%s - 0x1.0p-1075, double, RD);" % float.hex(x)
#    return float.fromhex(run_sollya(sollya_command))
#
#def next_double(x):
#    sollya_command = "display = hexadecimal!;"
#    sollya_command += "round(%s + 0x1.0p-1075, double, RU);" % float.hex(x)
#    return float.fromhex(run_sollya(sollya_command))

#def ulp(x):
#    str_x = float.hex(x)
#    exponent = int(str_x[str_x.find("p") + 1:])
#    # 0x1.0p-1074 is the smallest ULP(x)
#    if exponent < -1022:
#        exponent = -1022
#    str_ulp = "0x1.0p%d" % (exponent - 52)
#    return float.fromhex(str_ulp)

#def log_rn(x):
#    sollya_command = "display = hexadecimal!;"
#    sollya_command += "round(log(%s), double, RN);" % float.hex(x)
#    return float.fromhex(run_sollya(sollya_command))
#
#def tanh_rn(x):
#    sollya_command = "display = hexadecimal!;"
#    sollya_command += "round(tanh(%s), double, RN);" % float.hex(x)
#    return float.fromhex(run_sollya(sollya_command))

#def get_ulp_intervals(wF, str_invf, func_rn):
#    intervals = list()
#    for exponent in range(-2, -54, -1):
#        # We consider an interval where 2^exponent < tanh(x) <= 2^(exponent + 1)
#        # On this interval ULP(log(x)) == 2^(exponent - 52)
#        expected_ulp = float.fromhex("0x1p%d" % (exponent - (wF - 1)))
#        sollya_command = "display = hexadecimal!;"
#        sollya_command += "round(%s(-0x1.0p%d), double, RU);" % (str_invf, (exponent + 1))
#        sollya_command += "round(%s(-0x1.0p%d), double, RD);" % (str_invf, exponent)
#        bounds = run_sollya(sollya_command).split("\n")
#        lower = float.fromhex(bounds[0])
#        upper = float.fromhex(bounds[1])
#        if ulp(func_rn(lower)) != expected_ulp:
#            while ulp(func_rn(lower)) != expected_ulp:
#                lower = next_double(lower)
#        else:
#            prev_lower = prev_double(lower)
#            while ulp(func_rn(prev_lower)) == expected_ulp:
#                lower = prev_lower
#                prev_lower = prev_double(lower)
#        if ulp(func_rn(upper)) != expected_ulp:
#            while ulp(func_rn(upper)) != expected_ulp:
#                upper = prev_double(upper)
#        else:
#            next_upper = next_double(upper)
#            while ulp(func_rn(next_upper)) == expected_ulp:
#                upper = next_upper
#                next_upper = next_double(upper)
#        # Lower bound should fall be below sqrt(2)/2
#        #if lower < float.fromhex("0x1.6A09E667F3BCDp-1"):
#        #    lower = float.fromhex("0x1.6A09E667F3BCDp-1")
#        intervals.append((lower, upper, expected_ulp))
#    return intervals

def get_ulp_intervals2(debug = False, args, specs):
    intervals = list()
    for exponent in range(specs.exp_lower, specs.exp_upper + 1):
        expected_ulp = "0x1p%d" % (exponent - args.wF)
        sollya_command  = 'display = hexadecimal!;\n'
        sollya_command += "n = %d;\n" % (args.wF + 1)
        sollya_command += "f = %s;\n" % specs.func
        sollya_command += "invf = %s;\n" % specs.invf
        if specs.mono == Mono.INC:
            sollya_command += "lower_init = round(invf(0x1.0p%d), n, RU);\n" % exponent
            sollya_command += "upper_init = round(invf(0x1.0p%d - %s), n, RD);\n" % (exponent + 1, expected_ulp)
        else:
            sollya_command += "lower_init = round(invf(0x1.0p%d - %s), n, RU);\n" % (exponent + 1, expected_ulp)
            sollya_command += "upper_init = round(invf(0x1.0p%d), n, RD);\n" % exponent
        sollya_command += 'lower_ceil = lower_init;\n'
        sollya_command += 'if (lower_init != 0) then\n'
        sollya_command += '  lower_exp = floor(log2(abs(lower_init)))\n'
        sollya_command += 'else\n'
        sollya_command += '  lower_exp = -infty;\n'
        sollya_command += 'for d from 1 to (n-1) do {\n'
        sollya_command += '  guess = lower_ceil - 2^(lower_exp - d);\n'
        sollya_command += '  test = round(f(guess), n, RN);\n'
        if specs.mono == Mono.INC:
            sollya_command += "  if (test >= 0x1.0p%d) then \n" % exponent
        else:
            sollya_command += "  if (test < 0x1.0p%d) then \n" % exponent
        sollya_command += '    lower_ceil = guess;\n'
        sollya_command += '};\n'
        sollya_command += 'upper_ceil = upper_init;\n'
        sollya_command += 'if (upper_init != 0) then\n'
        sollya_command += '  upper_exp = floor(log2(abs(upper_init)))\n'
        sollya_command += 'else\n'
        sollya_command += '  upper_exp = -infty;\n'
        sollya_command += 'for d from 1 to (n-1) do {\n'
        sollya_command += '  guess = upper_ceil + 2^(upper_exp - d);\n'
        sollya_command += '  test = round(f(guess), n, RN);\n'
        if specs.mono == Mono.INC:
            sollya_command += "  if (test < 0x1.0p%d) then \n" % (exponent + 1)
        else:
            sollya_command += "  if (test >= 0x1.0p%d) then \n" % (exponent + 1)
        sollya_command += '    upper_ceil = guess;\n'
        sollya_command += '};\n'
        sollya_command += 'lower_ceil;\n'
        sollya_command += 'upper_ceil;\n'
        if not debug:
            bounds = run_sollya(sollya_command).split("\n")
            lower = bounds[0]
            upper = bounds[1]
            intervals.append((lower, upper, expected_ulp))
        else:
            eprint(sollya_command)
    eprint('{0:20} {1:20} {2:10}'.format('Intr.lo', 'Intr.up', 'Expected_ULP'))
    for (lower, upper, ulp) in intervals:
        eprint('{0:20} {1:20} {2:10}'.format(lower, upper, ulp))
    return intervals

def get_ulp_intervals(debug, wE, wF, exp_l, exp_h, func, func_inv, f_mono):
    intervals = list()
    for exponent in range(exp_l, exp_h + 1):
        expected_ulp = "0x1p%d" % (exponent - wF)
        sollya_command  = 'display = hexadecimal!;\n'
        sollya_command += "n = %d;\n" % (wF + 1)
        sollya_command += "f = %s;\n" % func
        sollya_command += "invf = %s;\n" % func_inv
        if f_mono == Mono.INC:
            sollya_command += "lower_init = round(invf(0x1.0p%d), n, RU);\n" % exponent
            sollya_command += "upper_init = round(invf(0x1.0p%d - %s), n, RD);\n" % (exponent + 1, expected_ulp)
        else:
            sollya_command += "lower_init = round(invf(0x1.0p%d - %s), n, RU);\n" % (exponent + 1, expected_ulp)
            sollya_command += "upper_init = round(invf(0x1.0p%d), n, RD);\n" % exponent
        sollya_command += 'lower_ceil = lower_init;\n'
        sollya_command += 'if (lower_init != 0) then\n'
        sollya_command += '  lower_exp = floor(log2(abs(lower_init)))\n'
        sollya_command += 'else\n'
        sollya_command += '  lower_exp = -infty;\n'
        #sollya_command += 'print("lo init", lower_init) >> "sollya.log";'
        #sollya_command += 'print("lo out", round(tanh(lower_init), n, RN)) >> "sollya.log";'
        #sollya_command += 'for d from 1 to ((n-1) - floor(log2(abs(lower_init)))) do {\n'
        sollya_command += 'for d from 1 to (n-1) do {\n'
        sollya_command += '  guess = lower_ceil - 2^(lower_exp - d);\n'
        sollya_command += '  test = round(f(guess), n, RN);\n'
        #sollya_command += 'print("lo guess", guess) >> "sollya.log";'
        #sollya_command += 'print("lo test", test) >> "sollya.log";'
        if f_mono == Mono.INC:
            sollya_command += "  if (test >= 0x1.0p%d) then \n" % exponent
        else:
            sollya_command += "  if (test < 0x1.0p%d) then \n" % exponent
        sollya_command += '    lower_ceil = guess;\n'
        sollya_command += '};\n'
        sollya_command += 'upper_ceil = upper_init;\n'
        sollya_command += 'if (upper_init != 0) then\n'
        sollya_command += '  upper_exp = floor(log2(abs(upper_init)))\n'
        sollya_command += 'else\n'
        sollya_command += '  upper_exp = -infty;\n'
        #sollya_command += 'print("up init", upper_init) >> "sollya.log";'
        #sollya_command += 'print("up out", round(tanh(upper_init), n, RN)) >> "sollya.log";'
        #sollya_command += 'for d from 1 to ((n-1) - floor(log2(abs(upper_init)))) do {\n'
        sollya_command += 'for d from 1 to (n-1) do {\n'
        sollya_command += '  guess = upper_ceil + 2^(upper_exp - d);\n'
        sollya_command += '  test = round(f(guess), n, RN);\n'
        #sollya_command += 'print("up guess", guess) >> "sollya.log";'
        #sollya_command += 'print("up test", test) >> "sollya.log";'
        if f_mono == Mono.INC:
            sollya_command += "  if (test < 0x1.0p%d) then \n" % (exponent + 1)
        else:
            sollya_command += "  if (test >= 0x1.0p%d) then \n" % (exponent + 1)
        sollya_command += '    upper_ceil = guess;\n'
        sollya_command += '};\n'
        sollya_command += 'lower_ceil;\n'
        sollya_command += 'upper_ceil;\n'
        if not debug:
            bounds = run_sollya(sollya_command).split("\n")
            lower = bounds[0]
            upper = bounds[1]
            intervals.append((lower, upper, expected_ulp))
        else:
            eprint(sollya_command)
    eprint('{0:20} {1:20} {2:10}'.format('Intr.lo', 'Intr.up', 'Expected_ULP'))
    for (lower, upper, ulp) in intervals:
        eprint('{0:20} {1:20} {2:10}'.format(lower, upper, ulp))
    return intervals

def find_upper_bound(wF, f_mono, func, limit):
    (f, invf) = func
    sollya_command  = 'display = decimal!;\n'
    sollya_command += "f = %s;\n" % f
    sollya_command += "invf = %s;\n" % invf
    sollya_command += "n = %d;\n" % (wF + 1)
    sollya_command += "r_ceil = %s;\n" % limit
    sollya_command += 'd_init = round(invf(r_ceil), n, RD);\n'
    sollya_command += 'd_ceil = d_init;\n'
    sollya_command += 'd_exp = floor(log2(abs(d_init)));\n'
    #sollya_command += 'for d from 1 to ((n-1) - floor(log2(d_init))) do {'
    sollya_command += 'for d from 1 to (n-1) do {\n'
    sollya_command += '  guess = d_ceil + 2^(d_exp - d);\n'
    sollya_command += '  test = round(f(guess), n, RN);\n'
    if f_mono == Mono.INC:
        sollya_command += "  if (test < %s) then \n" % limit
    else:
        sollya_command += "  if (test >= %s) then \n" % limit
    sollya_command += '    d_ceil = guess;\n'
    sollya_command += '};\n'
    sollya_command += 'd_ceil;\n'
    bound = run_sollya(sollya_command).split('\n')[0]
    return bound

def find_lower_bound(wF, f_mono, func, limit):
    (f, invf) = func
    sollya_command  = 'display = decimal!;\n'
    sollya_command += "f = %s;\n" % f
    sollya_command += "invf = %s;\n" % invf
    sollya_command += "n = %d;\n" % (wF + 1)
    sollya_command += "r_ceil = %s;\n" % limit
    sollya_command += 'd_init = round(invf(r_ceil), n, RU);\n'
    sollya_command += 'd_ceil = d_init;\n'
    sollya_command += 'd_exp = floor(log2(abs(d_init)));\n'
    #sollya_command += 'for d from 1 to ((n-1) - floor(log2(abs(d_init)))) do {\n'
    sollya_command += 'for d from 1 to (n-1) do {\n'
    sollya_command += '  guess = d_ceil - 2^(d_exp - d);\n'
    sollya_command += '  test = round(f(guess), n, RN);\n'
    if f_mono == Mono.INC:
        sollya_command += "  if (test >= %s) then \n" % limit
    else:
        sollya_command += "  if (test < %s) then \n" % limit
    sollya_command += '    d_ceil = guess;\n'
    sollya_command += '};\n'
    sollya_command += 'd_ceil;\n'
    bound = run_sollya(sollya_command).split('\n')[0]
    return bound
    #return sollya_command

def get_ulp_intervals_hardcode(wE, wF):
    if wE == 5 and wF == 10:
        intervals = [
                ('0x1p-15'   , '0x1.ffcp-15', '0x1p-25'),
                ('0x1p-14'   , '0x1.ffcp-14', '0x1p-24'),
                ('0x1p-13'   , '0x1.ffcp-13', '0x1p-23'),
                ('0x1p-12'   , '0x1.ffcp-12', '0x1p-22'),
                ('0x1p-11'   , '0x1.ffcp-11', '0x1p-21'),
                ('0x1p-10'   , '0x1.ffcp-10', '0x1p-20'),
                ('0x1p-9'    , '0x1.ffcp-9' , '0x1p-19'),
                ('0x1p-8'    , '0x1.ffcp-8' , '0x1p-18'),
                ('0x1p-7'    , '0x1.ffcp-7' , '0x1p-17'),
                ('0x1p-6'    , '0x1p-5'     , '0x1p-16'),
                ('0x1.004p-5', '0x1.004p-4' , '0x1p-15'),
                ('0x1.008p-4', '0x1.014p-3' , '0x1p-14'),
                ('0x1.018p-3', '0x1.054p-2' , '0x1p-13'),
                ('0x1.058p-2', '0x1.19p-1'  , '0x1p-12'),
                ('0x1.194p-1', '0x1.204p2'  , '0x1p-11')]
    #elif (wE == 8 and wF == 23):
    #elif (wE == 11 and wF == 52):
    else:
        intervals = []
    return intervals

def get_polys(args, f, q, bounds):
    (search_lo, search_up) = bounds
    sollya_command_head  = 'display = decimal!;\n'
    sollya_command_head += 'showmessagenumbers = on!;\n'
    sollya_command_head += 'suppressmessage(345, 346, 355, 376, 381);\n'
    sollya_command_head += "s_lo = %s;\n" % search_lo
    sollya_command_head += "s_up = %s;\n" % search_up
    sollya_command_head += "r = [s_lo; s_up];\n"
    sollya_command_head += "f = %s;\n" % f

    sollya_command_tail = ''
    if q is not None:
        if f == 'tanh(x)':
            # hack the left most poly to x*(1+x) for tanh, normal poly otherwise
            sollya_command_tail += 'if (s_lo == 0) then p = x * (0x1p0 + x)\n'
            sollya_command_tail += "else p = fpminimax(f, m, [|%d...|], r, absolute, floating, %s);\n" % (args.wF + 1, q)
        else:
            # normal poly if not tanh
            sollya_command_tail += "p = fpminimax(f, m, [|%d...|], r, absolute, floating, %s);\n" % (args.wF + 1, q)
    else:
        if f == 'tanh(x)' and not args.odd:
            # hack the left most poly to x*(1+x) for tanh, normal poly otherwise
            sollya_command_tail += 'if (s_lo == 0) then p = x * (0x1p0 + x)\n'
            sollya_command_tail += "else p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)
            # try hack in constrainted part instead of hard-coding
            #sollya_command_tail += "if (s_lo == 0) then p = fpminimax(f, [|3,...,m|], [|%d...|], r, absolute, floating, x*(1+x))\n" % (args.wF + 1)
        elif f == '1/(1+exp(-x))':
            sollya_command_tail += "p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)
            #sollya_command_tail += 'if (floor(log2(f(s_lo))) == -127) then p = taylor(f, 3, s_lo)\n'
            #sollya_command_tail += "else p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)
        else:
            sollya_command_tail += "p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)

    sollya_command_tail += 'p;\n'
    sollya_command_tail += 'display = hexadecimal!;\n'
    sollya_command_tail += 'p;\n'

    err_pattern = re.compile('Error \(369\).+')
    warn_pattern = re.compile('Warning \(\d+\).+')
    poly_dec = list()
    poly_hex = list()
    for deg in range(1, args.deg + 1)
        sollya_command = sollya_command_head
        if args.odd:
            sollya_command += 'm = [|0|];\n'
            sollya_command += "for i from 0 to floor((%d - 1)/2) do m[i + 1] = 2 * i + 1;\n" % deg
        else:
            sollya_command += "m = %d;\n" % deg
        sollya_command += sollya_command_tail 
        sollya_out = run_sollya(sollya_command).split('\n')
        remez_fail = err_pattern.match(sollya_out[0])
        remez_warn = warn_pattern.match(sollya_out[0])
        if (not remez_fail) and (not remez_warn):
            poly_dec.append(sollya_out[0])
            poly_hex.append(sollya_out[1])

    return (poly_dec, poly_hex)

def get_poly(args, f, bounds, q):
    (search_lo, search_up) = bounds
    sollya_command  = 'display = decimal!;\n'
    sollya_command += 'showmessagenumbers = on!;\n'
    sollya_command += 'suppressmessage(345, 346, 355, 376, 381);\n'
    sollya_command += "s_lo = %s;\n" % search_lo
    sollya_command += "s_up = %s;\n" % search_up
    sollya_command += "r = [s_lo; s_up];\n"
    sollya_command += "f = %s;\n" % f
    if args.odd:
        sollya_command += 'm = [|0|];\n'
        sollya_command += "for i from 0 to floor((%d - 1)/2) do m[i + 1] = 2 * i + 1;\n" % args.deg
    else:
        sollya_command += "m = %d;\n" % args.deg

    if q is not None:
        if f == 'tanh(x)':
            sollya_command += 'if (s_lo == 0) then p = x * (0x1p0 + x)\n'
            sollya_command += "else p = fpminimax(f, [|1,...,m|], [|%d...|], r, absolute, floating, %s);\n" % (args.wF + 1, q)
        else:
            sollya_command += "p = fpminimax(f, [|1,...,m|], [|%d...|], r, absolute, floating, %s);\n" % (args.wF + 1, q)
    else:
        if f == 'tanh(x)' and not args.odd:
            sollya_command += 'if (s_lo == 0) then p = x * (0x1p0 + x)\n'
            #sollya_command += "if (s_lo == 0) then p = fpminimax(f, [|3,...,m|], [|%d...|], r, absolute, floating, x*(1+x))\n" % (args.wF + 1)
            sollya_command += "else p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)
        elif f == '1/(1+exp(-x))':
            #sollya_command += 'if (floor(log2(f(s_lo))) == -127) then p = taylor(f, 3, s_lo)\n'
            #sollya_command += "else p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)
            sollya_command += "p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)
        else:
            sollya_command += "p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)

    sollya_command += 'p;\n'
    sollya_command += 'display = hexadecimal!;\n'
    sollya_command += 'p;\n'
    sollya_out = run_sollya(sollya_command).split('\n')
    err_pattern = re.compile('Error \(369\).+')
    warn_pattern = re.compile('Warning \(\d+\).+')
    remez_fail = err_pattern.match(sollya_out[0])
    remez_warn = warn_pattern.match(sollya_out[0])
    if (not remez_fail) and (not remez_warn):
        return (sollya_out[0], sollya_out[1])
    else:
        return None

def sollya_interval_err(bounds, inter, func, poly):
    (search_lo, search_up) = bounds
    (lower, upper, ulp) = inter
    #sollya_command  = 'display = hexadecimal!;\n'
    sollya_command  = "s_lo = %s;\n" % search_lo
    sollya_command += "s_up = %s;\n" % search_up
    sollya_command += "i_lo = %s;\n" % lower
    sollya_command += "i_up = %s;\n" % upper
    sollya_command += "f = %s;\n" % func
    sollya_command += "p = %s;\n" % poly
    sollya_command += 'if !((s_lo >= i_up) || (i_lo >= s_up)) then {\n'
    sollya_command += '  if (i_lo < s_lo) then lo = s_lo\n'
    sollya_command += '  else lo = i_lo;\n'
    sollya_command += '  if (i_up > s_up) then up = s_up\n'
    sollya_command += '  else up = i_up;\n'
    sollya_command += '  I = [lo; up];\n'
    sollya_command += "  ulp = %s;\n" % ulp
    sollya_command += '  print("true");\n'

    sollya_command += '  err = sup(supnorm(p, f, I, absolute, 1b-40));\n'
    sollya_command += '  err_ulp = err / ulp;\n'

    sollya_command += '  err_ulp;\n'
    sollya_command += '  I;\n'
    sollya_command += '  display = hexadecimal!;\n'
    sollya_command += '  ulp;\n'
    sollya_command += '}\n'
    sollya_command += 'else print("false");\n'
    sollya_out = run_sollya(sollya_command).split('\n')
    if sollya_out[0] == 'true':
        err_ulp = sollya_out[1]
        inter = sollya_out[2]
        ulp_hex = sollya_out[3]
        return (inter, ulp_hex, err_ulp)
    else:
        return None

def sollya_max(in0, in1):
    sollya_command  = 'showmessagenumbers = on!;\n'
    sollya_command += "%s > %s;\n" % (in0, in1)
    sollya_out = run_sollya(sollya_command).split('\n')
    if sollya_out[0] == 'true': 
        return in0
    else: 
        return in1

def sollya_less(a, b):
    sollya_command  = 'showmessagenumbers = on!;\n'
    sollya_command += 'suppressmessage(162);\n'
    sollya_command += "%s < %s;\n" % (a, b)
    sollya_out = run_sollya(sollya_command).split('\n')
    #eprint("sollya_less out was %s" % sollya_out[0])
    if sollya_out[0] == 'true': 
        return True
    else: 
        return False

def sollya_mean(args, target_lo, target_up):
    #sollya_command  = "m = round((%s + %s)/2, %d, RN);\n" % (target_lo, target_up, args.wF + 1)
    sollya_command  = "m = round((%s + %s)/2, %d, RN);\n" % (
            target_lo, target_up, math.floor((args.wF + 1)*0.7))
    #sollya_command  = "m = round((%s + %s)/2, 10, RN);\n" % (target_lo, target_up)
    sollya_command += 'm;\n'
    sollya_command += "%s < m && m < %s;\n" % (target_lo, target_up)
    sollya_out = run_sollya(sollya_command).split('\n')
    if sollya_out[1] == 'true': 
        return sollya_out[0]
    else: 
        return None

def check_mono(args, inc, x, poly0, poly1):
    sollya_command  = "p0 = %s;\n" % poly0
    sollya_command += "p1 = %s;\n" % poly1
    sollya_command += "out0 = round(p0(%s), %d, RN);\n" % (x, args.wF + 1)
    sollya_command += "out1 = round(p1(%s), %d, RN);\n" % (x, args.wF + 1)
    if inc == Mono.INC:
        sollya_command += 'out0 <= out1;\n'
    else:
        sollya_command += 'out0 >= out1;\n'
    sollya_out = run_sollya(sollya_command).split('\n')
    if sollya_out[0] == 'true': return True
    else: return False

def search_back(args, lower_min, lower_max, mono_steps):
    sollya_command  = "lo = %s;\n" % lower_min
    sollya_command += "hi = %s;\n" % lower_max
    sollya_command += "s = %s;\n" % mono_steps
    sollya_command += "w = %d;\n" % (args.wF + 1)
    sollya_command += 'm = round(hi - (hi - lo)*(s+1)/128, w, RN);\n' 
    sollya_command += 'm;\n'
    sollya_command += 'lo < m && m < hi;\n'
    sollya_out = run_sollya(sollya_command).split('\n')
    #eprint("search_back sollya_out[0] %s" % sollya_out[0])
    if sollya_out[1] == 'true': 
        return sollya_out[0]
    else: 
        return None

def adjust_mono(args, inc, x, poly0, poly1):
    sollya_command  = "p0 = %s;\n" % poly0
    sollya_command += "p1 = %s;\n" % poly1
    sollya_command += "out0 = round(p0(%s), %d, RN);\n" % (x, args.wF + 1)
    sollya_command += "out1 = round(p1(%s), %d, RN);\n" % (x, args.wF + 1)
    if inc:
        sollya_command += 'if (out0 > out1) then {\n'
        sollya_command += '  d = out0 - out1;\n'
    else:
        sollya_command += 'if (out0 < out1) then {\n'
        sollya_command += "  d = out1 - out0;\n" % x
    #sollya_command += "  r = round(d, %d, RU);\n" % (args.wF + 1)
    if inc: sollya_command += '  p2 = p1 + d;\n'
    else:   sollya_command += '  p2 = p1 - d;\n'

    sollya_command += '  print("true");\n'
    sollya_command += '  p2;\n'
    #if inc: sollya_command += "  round(p2(0), %d, RU);\n" % (args.wF + 1)
    #else:   sollya_command += "  round(p2(0), %d, RD);\n" % (args.wF + 1)
    sollya_command += "  round(p2(0), %d, RN);\n" % (args.wF + 1)

    sollya_command += '  display = hexadecimal!;\n'
    sollya_command += '  p2;\n'
    #if inc: sollya_command += "  round(p2(0), %d, RU);\n" % (args.wF + 1)
    #else:   sollya_command += "  round(p2(0), %d, RD);\n" % (args.wF + 1)
    sollya_command += "  round(p2(0), %d, RN);\n" % (args.wF + 1)

    sollya_command += '}\n'
    sollya_command += 'else print("false");\n'
    sollya_out = run_sollya(sollya_command).split('\n')

    #if sollya_out[0] == 'true': return ((sollya_out[1], sollya_out[2]), sollya_out[3])
    if sollya_out[0] == 'true': 
        poly_dec = sollya_out[1]
        poly_hex = sollya_out[3]
        c_dec = sollya_out[2] # const coef
        c_hex = sollya_out[4]
    #    hi_pattern = re.compile(' \+ x \* \([-.0-9e]+\)')
        coef_pattern = re.compile('[-.0-9a-fpx]+')
    #    poly_lo = hi_pattern.sub('', poly_dec, 1)
        #eprint("before sub: %s" % poly_dec)
        #eprint("after sub: %s" % poly_lo)
    #    q = coef_pattern.sub(c_dec, poly_lo, 1) # constraint part poly
        #eprint("after 2nd sub: %s" % q)
        poly_dec_adj = coef_pattern.sub(c_dec, poly_dec, 1)
        poly_hex_adj = coef_pattern.sub(c_hex, poly_hex, 1)
        return ((poly_dec_adj, poly_hex_adj), c_hex)
    else: 
        return None

def get_polys_errs(func, polys, intervals, seg_bounds):
    max_errs = list()
    for poly in polys:
        max_err = '0.0'
        for inter in intervals:
            err_tup = sollya_interval_err(seg_bounds, inter, func, poly)
            if err_tup is None: 
                continue
            eprint("interval %s, ulp %s, err(ulp) %s" % err_tup)
            max_err = sollya_max(max_err, err_tup[2])
            if sollya_less(str(args.approx_err), max_err):
                break # already over budget, just quit
        eprint("searched [%s;%s], max_err %s" % (seg_bounds[0], seg_bounds[1], max_err))
        #eprint("poly used: %s" % poly)
        max_errs.append(max_err)
    return max_errs

def search_func_seg2(args, specs, log_file):
    #specs.func
    #specs.invf
    #specs.mono
    #specs.exp_lower
    #specs.exp_upper
    #specs.bound_lower
    #specs.bound_upper
    eprint('starting get_ulp_intervals')
    #intervals = get_ulp_intervals(debug = False, args.wE, args.wF, specs.exp_lower, specs.exp_upper, specs.func, specs.invf, specs.mono)
    intervals = get_ulp_intervals2(debug = False, args, specs)
    eprint('finished get_ulp_intervals')

    seg_bounds = (specs.bound_lower, specs.bound_upper)
    while(): # tbd
        (polys_dec, polys_hex) = get_polys(args, specs.func, q = None, seg_bounds)
        max_errs = get_polys_errs(specs.func, polys_hex, intervals, seg_bounds)
        polys_met_err = [(e, p) for e, p in zip(max_errs, polys_hex) if sollya_less(e, str(args.approx_err)]
        # if empty list, shrink the segment and try again
        if not polys_met_err: 
            ret_mean = sollya_mean(args, seg_bounds[0], seg_bounds[1])
            if ret_mean is None: 
                # can't shrink, algo failed
                break
            seg_bounds[1] = ret_mean
            continue
        # otherwise, save the polys, and move on to next segment, or finish


def search_func_seg(args, f_mono, func, bounds, exps, log_file):
    (f, invf) = func
    (seg_lo, seg_up) = bounds
    (exp_lo, exp_up) = exps

    eprint('starting get_ulp_intervals')
    intervals = get_ulp_intervals(debug = False, args.wE, args.wF, exp_lo, exp_up, f, invf, f_mono)
    eprint('finished get_ulp_intervals')

    segments = list()
    if len(intervals) == 0: return (segments, True)

    tmp_segs = list()

    done = False
    failed = False
    adjusted = False
    shrink = False
    expand = False
    err_met = False
    commit = False
    flipped = False
    mono_steps = 0
    upper_min = seg_lo
    upper_max = seg_up
    old_max_err = 'infty'
    q = ''
    threshold = '* 8'
    cur_dir = Dir.SHRINK

    while (not done and not failed):
        if not adjusted:
            poly = get_poly(args, f, (seg_lo, seg_up), q = None)
        #else: 
        #    poly = get_poly(args, f, (seg_lo, seg_up), q)
        #    if poly is not None: 
        #        eprint("To  : %s" % poly[1])
        #    if poly[1] == poly_adj[1]: 
        #        shrink = True
        #    #adjusted = False
        if poly is None: 
            eprint("get_poly in [%s;%s] failed" % (seg_lo, seg_up))
            shrink = True
            #if cur_dir == Dir.SHRINK:
            #    expand = True
            #else:
            #    shrink = True
            #cur_dir = (cur_dir + 1) % 2
        #else:
        #    expand = False
        #    shrink = False
        #    adjusted = False
        #else: adjusted = False

        if not shrink and not expand:
            max_err = '0.0'
            for inter in intervals:
                err_tup = sollya_interval_err((seg_lo, seg_up), inter, f, poly[1])
                if err_tup is None: 
                    continue
                eprint("interval %s, ulp %s, err(ulp) %s" % err_tup)
                max_err = sollya_max(max_err, err_tup[2])
                if sollya_less(str(args.approx_err), max_err):
                    break # already over budget, just quit
                    
            eprint("searched [%s;%s], max_err %s" % (seg_lo, seg_up, max_err))
            eprint("poly used: %s" % poly[0])

            if sollya_less(max_err, str(args.approx_err)):
                err_met = True
                if len(segments) > 0 and not args.no_mono:
                    # check monotonicity of tmp_segs against previous seg
                    #adj_ret = adjust_mono(args, True, seg_lo, segments[-1][3][1], poly[1])
                    #if adj_ret is not None: 
                    #    if adjusted: # if need adjust again, just shrink
                    #        eprint('adjust mono failed, do shrink')
                    #        shrink = True
                    #        adjusted = False
                    #    else:
                    #        (poly_adj, q) = adj_ret
                    #        eprint('adjusted monotonicity:')
                    #        eprint("From: %s" % poly[1])
                    #        eprint("q   : %s" % q)
                    #        eprint("To  : %s" % poly_adj[1])
                    #        poly = poly_adj
                    #        adjusted = True
                    #        #continue
                    #else: 
                    #    expand = True
                    #    adjusted = False
                    mono = check_mono(args, True, seg_lo, segments[-1][3][1], poly[1])
                    if not mono:
                        target = search_back(args, lower_min, lower_max, mono_steps)
                        eprint("search_back range [%s;%s], steps %d" % (lower_min, lower_max, mono_steps))
                        mono_steps += 1
                        if target is None:
                            eprint('search_back failed')
                            shrink = True
                        else: 
                            eprint("search_back target %s" % target)
                            seg_lo = target
                    #else:
                        #eprint("mono passed, lower_min updated to %s" % lower_min)
                        #lower_min = seg_lo
                        #mono_steps = 0
                        #expand = True
                else: 
                    mono = True
                    #expand = True

                if mono:
                    eprint("mono passed")
                    expand = True
                    if len(tmp_segs) > 0: tmp_segs.pop()
                    tmp_segs.append((seg_lo, seg_up, max_err, poly))
                    old_max_err = 'infty'
            else: 
                err_met = False
                if not sollya_less(max_err, old_max_err + threshold):
                    threshold = '* 1'
                    cur_dir = (cur_dir + 1) % 2
                    upper_max = old_upper_max
                    eprint("flip dir, upper_max %s" % upper_max)
                    flipped = True
                if cur_dir == Dir.SHRINK: shrink = True
                else: expand = True
                #adjusted = False
            old_max_err = max_err

        if expand:
            mono_steps = 0
            old_upper_min = upper_min
            upper_min = seg_up
            target = sollya_mean(args, upper_min, upper_max)
            eprint("expand [%s;%s]" % (upper_min, upper_max))
            if flipped or err_met: eprint("upper_min %s" % upper_min)
            else: upper_min = old_upper_min
            if target is None: 
                eprint('Cannot expand, commit')
                commit = True
            else: 
                eprint("expand target %s" % target)
                seg_up = target
            expand = False
            flipped = False

        if shrink:
            mono_steps = 0
            old_upper_max = upper_max
            upper_max = seg_up
            target = sollya_mean(args, upper_min, upper_max)
            eprint("shrink [%s;%s]" % (upper_min, upper_max))
            if poly is None: upper_max = old_upper_max
            else: eprint("upper_max %s" % upper_max)
            if target is None: 
                eprint('Cannot shrink, commit')
                commit = True
            else: 
                eprint("shrink target %s" % target)
                seg_up = target
            shrink = False

        if commit:
            if len(tmp_segs) > 0:
                tmp_up = tmp_segs[0][1]
                if tmp_up == bounds[1]: done = True
                lower_min = tmp_segs[0][0]
                lower_max = tmp_segs[0][1]
                eprint("lower_min updated to %s" % lower_min)
                segments.append(tmp_segs.pop())
                seg_lo = tmp_up
                seg_up = bounds[1]
                upper_min = tmp_up
                upper_max = bounds[1]
                old_max_err = 'infty'
                cur_dir = Dir.SHRINK
                threshold = '* 8'
            else: failed = True
            commit = False

    log_file.write('{0:34} {1:34} {2:34} {3:5}\n'
            .format('Interval.Lower', 'Interval.Upper', 'Interval.Err_ULP', 'Mono vs last seg'))
    for (idx, (lower, upper, err, poly)) in enumerate(segments):
        if idx > 0: 
            mono = check_mono(args, True, lower, segments[idx-1][3][1], poly[1])
            #segments[idx-1][1] = lower # correct the modified bounds here
        else: 
            mono = True
        log_file.write('{0:34} {1:34} {2:34} {3:5}\n'.format(lower, upper, err, mono))

    log_file.write('\nPolynomials:\n')
    for (lower, upper, err, poly) in segments:
        log_file.write(poly[1] + '\n')
    log_file.write('\n')

    if (failed == True):
        log_file.write('!!! search segment algorithm failed !!!\n')
    else:
        log_file.write('!!! search segment algorithm succeed !!!\n')

    return (segments, failed)

def search_tanh_seg(args, bound, log_file):
    if args.wE == 5 and args.wF == 10:
    #or (wE == 8 and wF == 23) or (wE == 11 and wF == 52)
        intervals = get_ulp_intervals_hardcode(args.wE, args.wF)
    else:
        intervals = get_ulp_intervals(False, args.wE, args.wF, (1 - 2**(args.wE - 1)), -1, 'tanh(x)', 'atanh(x)', Mono.INC)
    #print('{0:23} {1:23} {2:23}'.format('Interval.Lower', 'Interval.Upper', 'Interval.ULP'))
    #for (lower, upper, ulp) in intervals:
    #    print('{0:23} {1:23} {2:23}'.format(lower, upper, ulp))
    segments = list()
    search_lo = '0'
    search_up = bound
    done = False
    failed = False
    tmp_segments = list()
    while (not done):
        sollya_command  = 'display = decimal!;\n'
        sollya_command += 'showmessagenumbers = on!;\n'
        sollya_command += 'suppressmessage(345, 346, 355, 376, 381);\n'
        sollya_command += "s_lo = %s;\n" % search_lo
        sollya_command += "s_up = %s;\n" % search_up
        sollya_command += "r = [s_lo; s_up];\n"
        sollya_command += 'f = tanh(x);\n'
        if args.odd:
            sollya_command += 'm = [|0|];\n'
            sollya_command += "for i from 0 to floor((%d - 1)/2) do m[i + 1] = 2 * i + 1;\n" % args.deg
        else:
            sollya_command += "m = %d;\n" % args.deg
        sollya_command += 'if (s_lo == 0) then p = x * (0x1p0 + x)\n'
        sollya_command += "else p = fpminimax(f, m, [|%d...|], r, absolute, floating);\n" % (args.wF + 1)
        sollya_command += 'max_err_ulp = 0;\n'
        check = 0
        for (lower, upper, ulp) in intervals:
            check += 1
            sollya_command += "//print(\"checkpoint %d\");\n" % check
            sollya_command += "i_lo = %s;\n" % lower
            sollya_command += "i_up = %s;\n" % upper
            sollya_command += 'if !((s_lo >= i_up) || (i_lo >= s_up)) then {\n'
            sollya_command += '  if (i_lo < s_lo) then lo = s_lo\n'
            sollya_command += '  else lo = i_lo;\n'
            sollya_command += '  if (i_up > s_up) then up = s_up\n'
            sollya_command += '  else up = i_up;\n'
            sollya_command += '  I = [lo; up];\n'
            sollya_command += "  ulp = %s;\n" % ulp
            sollya_command += '  err = sup(supnorm(p, f, I, absolute, 1b-40));\n'
            sollya_command += '  err_ulp = err / ulp;\n'
            sollya_command += '  if (err_ulp > max_err_ulp) then {\n'
            sollya_command += '    max_err_ulp = err_ulp;\n'
            sollya_command += '    max_i = I;\n'
            sollya_command += '  };\n'
            sollya_command += '};\n'
            #sollya_command += 'else print("Search ", s_lo, ";", s_up, ' \ 
            #'"range skipped: [", i_lo, ";", i_up, "]") >> "sollya.log";'
        sollya_command += 'max_err_ulp;\n'
        sollya_command += 'p;\n'
        sollya_command += 'max_i;\n'
        sollya_file = open('last.sollya', 'w')
        sollya_file.write(sollya_command)
        sollya_file.close()
        #eprint('Starting Sollya')
        sollya_out = run_sollya(sollya_command).split('\n')
        eprint("searching [%s;%s], interval %s, max_err_ulp %s" % 
                (search_lo, search_up, sollya_out[2], sollya_out[0]))
        err_pattern = re.compile('Error \(369\).+')
        warn_pattern = re.compile('Warning \(\d+\).+')
        remez_fail = err_pattern.match(sollya_out[0])
        remez_warn = warn_pattern.match(sollya_out[0])
        if remez_warn:
            eprint('-----------\n')
            for line in sollya_out:
                eprint(line)
            eprint('-----------\n')
            sollya_file = open('warning.sollya', 'w')
            sollya_file.write(sollya_command)
            sollya_file.close()
        if (not remez_fail) and (not remez_warn):
            max_err = float(sollya_out[0])
            #max_err = float.fromhex(sollya_out[0])
        else:
            max_err = args.approx_err
        #if max_err < approx_err:
        #    # commit this interval and move to next
        #    #print(sollya_out[1])
        #    segments.append((search_lo, search_up, max_err, sollya_out[1]))
        #    if search_up != bound:
        #        search_lo = search_up
        #        search_up = bound
        #    else:
        #        # finished, quit the loop
        #        done = True
        #else:
        #    # shrink interval
        #    sollya_command  = "m = round((%s + %s)/2, 7, RN);" % (search_lo, search_up)
        #    sollya_command += 'm;'
        #    sollya_command += "%s < m && m < %s;" % (search_lo, search_up)
        #    #eprint(sollya_command)
        #    sollya_out = run_sollya(sollya_command).split('\n')
        #    #eprint("search_up \"%s\", shrink to \"%s\"" % (search_up, sollya_out[0]))
        #    #eprint("sollya_out[1] is %s" % sollya_out[1])
        #    #if (search_up != sollya_out[0]):
        #    if (sollya_out[1] == 'true'):
        #        search_up = sollya_out[0]
        #    else:
        #        done = True
        #        failed = True
        if max_err < args.approx_err:
            if len(tmp_segments) == 1:
                tmp_segments.pop()
            tmp_segments.append((search_lo, search_up, max_err, sollya_out[1]))
            sollya_command  = "m = round((%s + %s)/2, 7, RN);" % (search_up, failed_bound)
            sollya_command += 'm;'
            sollya_command += "%s < m;" % search_up
            sollya_out = run_sollya(sollya_command).split('\n')
            if (sollya_out[1] == 'true'):
                search_up = sollya_out[0]
            else:
                segments.append(tmp_segments.pop())
                if search_up != bound:
                    search_lo = search_up
                    search_up = bound
                else:
                    # finished, quit the loop
                    done = True
        else:
            failed_bound = search_up
            if len(tmp_segments) == 0:
                sollya_command  = "m = round((%s + %s)/2, 7, RN);" % (search_lo, search_up)
                sollya_command += 'm;'
                sollya_command += "%s < m && m < %s;" % (search_lo, search_up)
                sollya_out = run_sollya(sollya_command).split('\n')
                if (sollya_out[1] == 'true'):
                    search_up = sollya_out[0]
                else:
                    done = True
                    failed = True
            else:
                tmp_up = tmp_segments[0][1]
                eprint("tmp_up %s, search_up %s" % (tmp_up, search_up))
                sollya_command  = "m = round((%s + %s)/2, 7, RN);" % (tmp_up, search_up)
                sollya_command += 'm;'
                sollya_command += "%s < m && m < %s;" % (tmp_up, search_up)
                sollya_out = run_sollya(sollya_command).split('\n')
                eprint("sollya_out[0] %s, sollya_out[1] %s" % (sollya_out[0], sollya_out[1]))
                if (sollya_out[1] == 'true'):
                    search_up = sollya_out[0]
                else:
                    segments.append(tmp_segments.pop())
                    if search_up != bound:
                        search_lo = tmp_up
                        search_up = bound
                    else:
                        # finished, quit the loop
                        done = True
    log_file.write('{0:23} {1:23} {2:23}\n'.format('Interval.Lower', 'Interval.Upper', 'Interval.Err_ULP'))
    #print('{0:23} {1:23} {2:23}'.format('Interval.Lower', 'Interval.Upper', 'Interval.Err_ULP'))
    for (lower, upper, err, poly) in segments:
        log_file.write('{0:23} {1:23} {2:23}\n'.format(lower, upper, err))
        #print('{0:23} {1:23} {2:23}'.format(lower, upper, err))
    for (lower, upper, err, poly) in segments:
        log_file.write(poly + '\n')
        #print(poly)
    if (failed == True):
        log_file.write('!!! Can\'t shrink the last segment, algorithm failed !!!\n')
        #print('!!! Can\'t shrink the last segment, algorithm failed !!!')
    return (segments, failed)

def horner2coef(horner):
    coef = list()
    pattern = re.compile('(?:^|[^^])([-.0-9e]+)')
    #pattern = re.compile('([-.0-9e]+) \+ x \* \(')
    mlist = pattern.finditer(horner)
    for m in mlist:
        coef.append(m.group(1))
        #eprint("horner2coef coef: %s" % m.group(1))
    #eprint("len(coef) is %d" % len(coef))
    return coef

def create_mult_file(args):
    #mult_wf = (args.wF * 2 + 1)
    mult_wf = args.wF
    flopoco_out = run_flopoco(['FPMult', "wE=%d" % args.wE, "wF=%d" % args.wF, 
        "wFout=%d" % mult_wf]).split('\n')
    inst_pattern = re.compile('Entity (FPMult[0-9a-z_]+)')
    pipe_pattern = re.compile('Pipeline depth = (\d+)')
    mult_name = 'no_mult_name'
    mult_pipe = -1
    for idx, l in enumerate(flopoco_out):
        eprint("flopoco_out line %d: %s" % (idx, l))
        inst_match = inst_pattern.match(l)
        if inst_match:
            #eprint('inst matched mult!')
            mult_name = inst_match.group(1)
            mult_pipe = int(pipe_pattern.search(flopoco_out[idx + 1]).group(1))
    os.rename('./flopoco.vhdl', './' + mult_name + '.vhdl')
    return (mult_name, mult_pipe)

def create_add_file(args):
    add_wf = args.wF
    flopoco_out = run_flopoco(['FPAdd', "wE=%d" % args.wE, "wF=%d" % add_wf]).split('\n')
    inst_pattern = re.compile('Entity (FPAdd[0-9a-z_]+)')
    pipe_pattern = re.compile('Pipeline depth = (\d+)')
    add_name = 'no_add_name'
    add_pipe = -1
    for idx, l in enumerate(flopoco_out):
        eprint("flopoco_out line %d: %s" % (idx, l))
        inst_match = inst_pattern.match(l)
        if inst_match:
            #eprint('inst matched add!')
            add_name = inst_match.group(1)
            add_pipe = int(pipe_pattern.search(flopoco_out[idx + 1]).group(1))
    os.rename('./flopoco.vhdl', './' + add_name + '.vhdl')
    return (add_name, add_pipe)

def create_flopoco_file(args, param_list):
    flopoco_out = run_flopoco(param_list).split('\n')
    inst_pattern = re.compile("Entity (%s[0-9a-z_]+)" % param_list[0])
    pipe_pattern = re.compile('Pipeline depth = (\d+)')
    op_name = 'no_op_name'
    op_pipe = -1
    for idx, l in enumerate(flopoco_out):
        eprint("flopoco_out line %d: %s" % (idx, l))
        inst_match = inst_pattern.match(l)
        if inst_match:
            #eprint('inst matched mult!')
            op_name = inst_match.group(1)
            op_pipe = int(pipe_pattern.search(flopoco_out[idx + 1]).group(1))
    os.rename('./flopoco.vhdl', './' + op_name + '.vhdl')
    return (op_name, op_pipe)

def print_assign_rom(args, num_segs, rom_addr_width, rom_content):
    line = ''
    if args.odd: coef_count = int(math.ceil(args.deg/2)) + 1
    else: coef_count = args.deg + 1
    eprint("coef_count is %d, args.deg %d" % (coef_count, args.deg))
    for d in range(coef_count):
        # generate deg number of ROMs
        line += '// ROM declaration of coefficient of minimax-polynomial\n'
        line += print_header_rom(args.wE, args.wF, "%s_rom_%d" % (args.func, d), rom_addr_width)
        line += 'always @(posedge aclk) begin\n'
        line += '  case (addr)\n'
        for s in range(num_segs + 1):
            # skipped x*(1+x) in coef so range < num_segs - 1
            # updated: don't skip segment 0
            # updated: segment + 1 to include top boundary
            #eprint("assign rom dimension %d %d" % (s, d))
            #eprint("rom_content %s" % rom_content[s][d])
            coef_bin = run_fp2bin([str(args.wE), str(args.wF), rom_content[s][d]]).split('\n')[0]
            line += "    \'d{0:02}:    data <= \'b{1}; // {2}\n".format(s, coef_bin, rom_content[s][d])
            #line += "    \'d%d: data <= \'b%s;\n" % (s, coef_bin)
        line += '    default: data <= \'b0;\n'
        line += '  endcase\n'
        line += 'end\n'
        line += '\n'
        line += 'endmodule\n'
        line += "// %s_rom_%d\n" % (args.func, d)
        line += '\n'
    return line

class horner_param:
    def __init__(self, symmetry, num_segs, cmp_pipe, decode_pipe, 
            reg_mult, reg_add, reg_sqr, mult_name, mult_pipe, 
            add_name, add_pipe, sqr_name, sqr_pipe):
        self.symmetry = symmetry
        self.num_segs = num_segs
        self.cmp_pipe = cmp_pipe 
        self.decode_pipe = decode_pipe
        self.reg_mult = reg_mult
        self.reg_add = reg_add
        self.reg_sqr = reg_sqr
        self.mult_name = mult_name
        self.mult_pipe = mult_pipe
        self.add_name = add_name
        self.add_pipe = add_pipe
        self.sqr_name = sqr_name
        self.sqr_pipe = sqr_pipe

def print_horner_func(args, params, segments):
    num_segs    = params.num_segs      
    cmp_pipe    = params.cmp_pipe      
    decode_pipe = params.decode_pipe   
    reg_mult    = params.reg_mult      
    reg_add     = params.reg_add       
    mult_name   = params.mult_name     
    mult_pipe   = params.mult_pipe     
    add_name    = params.add_name      
    add_pipe    = params.add_pipe      
    # 1 for first ROM delay
    data_pipe = (cmp_pipe + decode_pipe + 1 + (args.deg - 1) * 
            (mult_pipe + reg_mult + add_pipe + reg_add))
    rom_addr_pipe = (args.deg * (mult_pipe + reg_mult) + (args.deg - 1) * 
            (add_pipe + reg_add))
    valid_pipe = (cmp_pipe + decode_pipe + 1 + args.deg * 
            (mult_pipe + reg_mult + add_pipe + reg_add))
    rom_addr_width = math.ceil(math.log(num_segs + 1, 2))

    line  = ''
    line += 'wire [WE+WF+2:0] absA;\n'
    line += "reg  [WE+WF+2:0] absA_d [0:%d];\n" % data_pipe
    line += "wire [%d:0] leq_seg;\n" % (num_segs - 1)
    line += "reg  [%d:0] rom_addr;\n" % (rom_addr_width - 1)
    line += "reg  [%d:0] rom_addr_d [0:%d];\n" % (rom_addr_width - 1, rom_addr_pipe)
    line += "reg  [%d:0] a_vld_d;\n" % valid_pipe
    if params.symmetry == Symmetry.ODD:
        line += "reg  [%d:0] sign_a_d;\n" % valid_pipe
    for d in range(args.deg + 1):
        line += "wire [WE+WF+2:0] rom%d_out;\n" % d
    for d in range(args.deg):
        line += "wire [WE+WF+2:0] mult%d_out;\n" % d
        line += "wire [WE+WF+2:0] add%d_out;\n" % d
        line += "reg  [WE+WF+2:0] mult%d_out_d [0:%d];\n" % (d, reg_mult)
        line += "reg  [WE+WF+2:0] add%d_out_d [0:%d];\n" % (d, reg_add)
    line += '\n'
    line += 'wire [1:0]    excpt_r;\n'
    line += 'wire          sign_r;\n'
    line += 'wire [WE-1:0] expo_r;\n'
    line += 'wire [WF-1:0] manti_r;\n'
    line += 'wire          dummy;\n'
    line += '\n'
    line += print_wire_in1()
    line += 'assign R_TDATA = {excpt_r, sign_r, expo_r, manti_r};\n'
    line += "assign R_TVALID = a_vld_d[%d];\n" % valid_pipe
    line += '\n'

    if params.symmetry == Symmetry.NO:
        line += "assign {excpt_r, sign_r, expo_r, manti_r} = add0_out;\n" 
        line += 'assign absA = {excpt_a, sign_a, expo_a, manti_a};\n'
    elif params.symmetry == Symmetry.ODD:
        line += "assign sign_r = sign_a_d[%d];\n" % valid_pipe
        line += "assign {excpt_r, dummy, expo_r, manti_r} = add0_out;\n" 
        line += 'assign absA = {excpt_a, 1\'b0, expo_a, manti_a};\n'
    elif params.symmetry == Symmetry.EVEN:
        line += "assign {excpt_r, sign_r, expo_r, manti_r} = add0_out;\n" 
        line += 'assign absA = {excpt_a, 1\'b0, expo_a, manti_a};\n'
    line += '\n'

    line += 'always @(*) begin\n'
    line += '  absA_d[0] <= absA;\n'
    line += '  rom_addr_d[0] <= rom_addr;\n'
    line += '  a_vld_d[0] <= A_TVALID;\n'
    if params.symmetry == Symmetry.ODD:
        line += '  sign_a_d[0] <= sign_a;\n'
    for d in range(args.deg):
        line += "  mult%d_out_d[0] <= mult%d_out;\n" % (d, d)
        line += "  add%d_out_d[0] <= add%d_out;\n" % (d, d)
    line += 'end\n'
    line += '\n'

    if reg_mult == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '    if (~aresetn) begin\n'
        for d in range(args.deg):
            line += "      mult%d_out_d[1] <= \'b0;\n" % d
        line += '    end\n'
        line += '    else begin\n'
        for d in range(args.deg):
            line += "      mult%d_out_d[1] <= mult%d_out_d[0];\n" % (d, d)
        line += '    end\n'
        line += 'end\n'

    if reg_add == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '    if (~aresetn) begin\n'
        for d in range(args.deg):
            line += "      add%d_out_d[1] <= \'b0;\n" % d
        line += '    end\n'
        line += '    else begin\n'
        for d in range(args.deg):
            line += "      add%d_out_d[1] <= add%d_out_d[0];\n" % (d, d)
        line += '    end\n'
        line += 'end\n'

    line += 'genvar i;\n'
    line += 'generate\n'
    line += "for (i = 1; i <= %d; i = i + 1) begin : gen_data_pipe\n" % data_pipe
    line += '  always @(posedge aclk or negedge aresetn) begin\n'
    line += '    if (~aresetn) begin\n'
    line += '      absA_d[i] <= \'b0;\n'
    line += '    end\n'
    line += '    else begin\n'
    line += '      absA_d[i] <= absA_d[i-1];\n'
    line += '    end\n'
    line += '  end\n'
    line += 'end\n'
    line += "for (i = 1; i <= %d; i = i + 1) begin : gen_rom_addr_pipe\n" % rom_addr_pipe
    line += '  always @(posedge aclk or negedge aresetn) begin\n'
    line += '    if (~aresetn) begin\n'
    line += '      rom_addr_d[i] <= \'b0;\n'
    line += '    end\n'
    line += '    else begin\n'
    line += '      rom_addr_d[i] <= rom_addr_d[i-1];\n'
    line += '    end\n'
    line += '  end\n'
    line += 'end\n'
    line += "for (i = 1; i <= %d; i = i + 1) begin : gen_valid_pipe\n" % valid_pipe
    line += '  always @(posedge aclk or negedge aresetn) begin\n'
    line += '    if (~aresetn) begin\n'
    line += '      a_vld_d[i] <= \'b0;\n'
    if params.symmetry == Symmetry.ODD:
        line += '      sign_a_d[i] <= \'b0;\n'
    line += '    end\n'
    line += '    else begin\n'
    line += '      a_vld_d[i] <= a_vld_d[i-1];\n'
    if params.symmetry == Symmetry.ODD:
        line += '      sign_a_d[i] <= sign_a_d[i-1];\n'
    line += '    end\n'
    line += '  end\n'
    line += 'end\n'
    line += 'endgenerate\n'
    line += '\n'

    for seg in range(num_segs):
        upper = run_fp2bin([str(args.wE), str(args.wF), segments[seg][1]]).split('\n')[0]
        line += "fp_cmp_leq_p%d cmp_leq_inst_seg%d\n" % (cmp_pipe, seg)
        line += '(\n'
        line += '  .aclk        (aclk),\n'
        line += '  .aresetn     (aresetn),\n'
        line += '  .A_TDATA     (absA),\n'
        line += '  .A_TVALID    (A_TVALID),\n'
        line += "  .B_TDATA     (\'b%s), // %s\n" % (upper, segments[seg][1])
        line += '  .B_TVALID    (1\'b1),\n'
        line += "  .R_TDATA     (leq_seg[%d]),\n" % seg
        line += '  .R_TVALID    ()\n'
        line += ');\n'
        line += '\n'

    if decode_pipe == 1: 
        line += 'always @(posedge aclk) begin\n'
    else: 
        line += 'always @(*) begin\n'
    line += '  case(leq_seg)\n'
    ones = ''
    for seg in range(num_segs + 1):
        line += "    \'b%s: rom_addr <= \'d%d;\n" % (ones.rjust(num_segs, '1'), seg)
        ones += '0'
    line += '    default: rom_addr <= \'b0;\n'
    line += '  endcase\n'
    line += 'end\n'
    line += '\n'

    rom_name = "%s_rom_%d" % (args.func, args.deg)
    line += "%s %s_inst\n" % (rom_name, rom_name)
    line += '(\n'
    line += '  .aclk    (aclk),\n'
    line += '  .addr    (rom_addr),\n' 
    line += "  .data    (rom%d_out)\n" % args.deg
    line += ');\n'
    line += '\n'

    cur_pipe = cmp_pipe + decode_pipe + 1 # 1 for first ROM
    for d in reversed(range(args.deg)):
        line += "%s %s_inst_%d\n" % (mult_name, mult_name, d)
        line += '(\n'
        line += '  .clk    (aclk),\n'
        line += '  .rst    (~aresetn),\n'
        line += '  .X      (absA_d[%d]),\n' % cur_pipe
        if d == args.deg - 1:
            # highest degree from ROM
            line += "  .Y      (rom%d_out),\n" % args.deg
        else:
            # otherwise from higher deg add output
            line += "  .Y      (add%d_out_d[%d]),\n" % (d + 1, reg_add)
        line += "  .R      (mult%d_out)\n" % d
        line += ');\n'
        line += '\n'
        cur_pipe = cur_pipe + mult_pipe + reg_mult

        rom_name = "%s_rom_%d" % (args.func, d)
        line += "%s %s_inst\n" % (rom_name, rom_name)
        line += '(\n'
        line += '  .aclk    (aclk),\n'
        line += "  .addr    (rom_addr_d[%d]),\n" %  (cur_pipe - cmp_pipe - decode_pipe - 1)
        line += "  .data    (rom%d_out)\n" % d
        line += ');\n'
        line += '\n'

        line += "%s %s_inst_%d\n" % (add_name, add_name, d)
        line += '(\n'
        line += '  .clk    (aclk),\n'
        line += '  .rst    (~aresetn),\n'
        line += "  .X      (mult%d_out_d[%d]),\n" % (d, reg_mult)
        line += "  .Y      (rom%d_out),\n" % d
        line += "  .R      (add%d_out)\n" % d
        line += ');\n'
        line += '\n'
        cur_pipe = cur_pipe + add_pipe + reg_add
    line += '\n'
    return line

def print_horner_odd_pwr(args, params, segments):
    num_segs    = params.num_segs      
    cmp_pipe    = params.cmp_pipe      
    decode_pipe = params.decode_pipe   
    reg_mult    = params.reg_mult      
    reg_add     = params.reg_add       
    reg_sqr     = params.reg_sqr       
    mult_name   = params.mult_name     
    mult_pipe   = params.mult_pipe     
    add_name    = params.add_name      
    add_pipe    = params.add_pipe      
    sqr_name    = params.sqr_name      
    sqr_pipe    = params.sqr_pipe      

    mac_count = int(math.ceil(args.deg/2))
    coef_count = mac_count + 1
    
    if (sqr_pipe + reg_sqr) > (cmp_pipe + decode_pipe + 1):
        start_pipe = (sqr_pipe + reg_sqr)
        cmp_ex_pipe = start_pipe - (cmp_pipe + decode_pipe + 1)
        sqr_ex_pipe = 0
    else:
        start_pipe = (cmp_pipe + decode_pipe + 1)
        sqr_ex_pipe = start_pipe - (sqr_pipe + reg_sqr)
        cmp_ex_pipe = 0

    # 1 for first ROM delay
    #data_pipe = (cmp_pipe + decode_pipe + 1 + 
    data_pipe = (start_pipe + 
            (mac_count - 1) * (mult_pipe + reg_mult + add_pipe + reg_add))

    rom_addr_pipe = (mac_count * (mult_pipe + reg_mult) + 
            (mac_count - 1) * (add_pipe + reg_add))

    #valid_pipe = (cmp_pipe + decode_pipe + 1 + 
    valid_pipe = (start_pipe + 
            mac_count * (mult_pipe + reg_mult + add_pipe + reg_add))

    sqr_out_pipe = (reg_sqr + 
            (mac_count - 2) * (mult_pipe + reg_mult + add_pipe + reg_add))

    rom_addr_width = math.ceil(math.log(num_segs + 1, 2))

    line  = ''
    line += 'wire [WE+WF+2:0] absA;\n'
    line += "reg  [WE+WF+2:0] absA_d [0:%d];\n" % data_pipe
    line += "wire [%d:0] leq_seg;\n" % (num_segs - 1)
    line += "reg  [%d:0] rom_addr;\n" % (rom_addr_width - 1)
    line += "reg  [%d:0] rom_addr_d [0:%d];\n" % (rom_addr_width - 1, rom_addr_pipe)
    line += "reg  [%d:0] a_vld_d;\n" % valid_pipe
    if params.symmetry == Symmetry.ODD:
        line += "reg  [%d:0] sign_a_d;\n" % valid_pipe
    for d in range(coef_count):
        line += "wire [WE+WF+2:0] rom%d_out;\n" % d
    for d in range(mac_count):
        line += "wire [WE+WF+2:0] mult%d_out;\n" % d
        line += "wire [WE+WF+2:0] add%d_out;\n" % d
        line += "reg  [WE+WF+2:0] mult%d_out_d [0:%d];\n" % (d, reg_mult)
        line += "reg  [WE+WF+2:0] add%d_out_d [0:%d];\n" % (d, reg_add)
    line += 'wire [WE+WF+2:0] sqr_out;\n'
    line += "reg  [WE+WF+2:0] sqr_out_d [0:%d];\n" % sqr_out_pipe
    line += '\n'
    line += 'wire [1:0]    excpt_r;\n'
    line += 'wire          sign_r;\n'
    line += 'wire [WE-1:0] expo_r;\n'
    line += 'wire [WF-1:0] manti_r;\n'
    line += 'wire          dummy;\n'
    line += '\n'
    line += print_wire_in1()
    line += 'assign R_TDATA = {excpt_r, sign_r, expo_r, manti_r};\n'
    line += "assign R_TVALID = a_vld_d[%d];\n" % valid_pipe
    line += '\n'

    if params.symmetry == Symmetry.NO:
        line += "assign {excpt_r, sign_r, expo_r, manti_r} = add0_out;\n" 
        line += 'assign absA = {excpt_a, sign_a, expo_a, manti_a};\n'
    elif params.symmetry == Symmetry.ODD:
        line += "assign sign_r = sign_a_d[%d];\n" % valid_pipe
        line += "assign {excpt_r, dummy, expo_r, manti_r} = add0_out;\n" 
        line += 'assign absA = {excpt_a, 1\'b0, expo_a, manti_a};\n'
    elif params.symmetry == Symmetry.EVEN:
        line += "assign {excpt_r, sign_r, expo_r, manti_r} = add0_out;\n" 
        line += 'assign absA = {excpt_a, 1\'b0, expo_a, manti_a};\n'
    line += '\n'

    line += 'always @(*) begin\n'
    line += '  absA_d[0] <= absA;\n'
    line += '  rom_addr_d[0] <= rom_addr;\n'
    line += '  a_vld_d[0] <= A_TVALID;\n'
    line += '  sqr_out_d[0] <= sqr_out;\n'
    if params.symmetry == Symmetry.ODD:
        line += '  sign_a_d[0] <= sign_a;\n'
    for d in range(mac_count):
        line += "  mult%d_out_d[0] <= mult%d_out;\n" % (d, d)
        line += "  add%d_out_d[0] <= add%d_out;\n" % (d, d)
    line += 'end\n'
    line += '\n'

    if reg_mult == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '    if (~aresetn) begin\n'
        for d in range(mac_count):
            line += "      mult%d_out_d[1] <= \'b0;\n" % d
        line += '    end\n'
        line += '    else begin\n'
        for d in range(mac_count):
            line += "      mult%d_out_d[1] <= mult%d_out_d[0];\n" % (d, d)
        line += '    end\n'
        line += 'end\n'

    if reg_add == 1:
        line += 'always @(posedge aclk or negedge aresetn) begin\n'
        line += '    if (~aresetn) begin\n'
        for d in range(mac_count):
            line += "      add%d_out_d[1] <= \'b0;\n" % d
        line += '    end\n'
        line += '    else begin\n'
        for d in range(mac_count):
            line += "      add%d_out_d[1] <= add%d_out_d[0];\n" % (d, d)
        line += '    end\n'
        line += 'end\n'

    #if reg_sqr == 1:
    #    line += 'always @(posedge aclk or negedge aresetn) begin\n'
    #    line += '    if (~aresetn) begin\n'
    #    line += '      sqr_out_d[1] <= \'b0;\n'
    #    line += '    end\n'
    #    line += '    else begin\n'
    #    line += '      sqr_out_d[1] <= sqr_out_d[0];\n'
    #    line += '    end\n'
    #    line += 'end\n'

    line += 'genvar i;\n'
    line += 'generate\n'
    line += "for (i = 1; i <= %d; i = i + 1) begin : gen_data_pipe\n" % data_pipe
    line += '  always @(posedge aclk or negedge aresetn) begin\n'
    line += '    if (~aresetn) begin\n'
    line += '      absA_d[i] <= \'b0;\n'
    line += '    end\n'
    line += '    else begin\n'
    line += '      absA_d[i] <= absA_d[i-1];\n'
    line += '    end\n'
    line += '  end\n'
    line += 'end\n'
    line += "for (i = 1; i <= %d; i = i + 1) begin : gen_sqr_out_pipe\n" % sqr_out_pipe
    line += '  always @(posedge aclk or negedge aresetn) begin\n'
    line += '    if (~aresetn) begin\n'
    line += '      sqr_out_d[i] <= \'b0;\n'
    line += '    end\n'
    line += '    else begin\n'
    line += '      sqr_out_d[i] <= sqr_out_d[i-1];\n'
    line += '    end\n'
    line += '  end\n'
    line += 'end\n'
    line += "for (i = 1; i <= %d; i = i + 1) begin : gen_rom_addr_pipe\n" % rom_addr_pipe
    line += '  always @(posedge aclk or negedge aresetn) begin\n'
    line += '    if (~aresetn) begin\n'
    line += '      rom_addr_d[i] <= \'b0;\n'
    line += '    end\n'
    line += '    else begin\n'
    line += '      rom_addr_d[i] <= rom_addr_d[i-1];\n'
    line += '    end\n'
    line += '  end\n'
    line += 'end\n'
    line += "for (i = 1; i <= %d; i = i + 1) begin : gen_valid_pipe\n" % valid_pipe
    line += '  always @(posedge aclk or negedge aresetn) begin\n'
    line += '    if (~aresetn) begin\n'
    line += '      a_vld_d[i] <= \'b0;\n'
    if params.symmetry == Symmetry.ODD:
        line += '      sign_a_d[i] <= \'b0;\n'
    line += '    end\n'
    line += '    else begin\n'
    line += '      a_vld_d[i] <= a_vld_d[i-1];\n'
    if params.symmetry == Symmetry.ODD:
        line += '      sign_a_d[i] <= sign_a_d[i-1];\n'
    line += '    end\n'
    line += '  end\n'
    line += 'end\n'
    line += 'endgenerate\n'
    line += '\n'

    for seg in range(num_segs):
        upper = run_fp2bin([str(args.wE), str(args.wF), segments[seg][1]]).split('\n')[0]
        line += "fp_cmp_leq_p%d cmp_leq_inst_seg%d\n" % (cmp_pipe, seg)
        line += '(\n'
        line += '  .aclk        (aclk),\n'
        line += '  .aresetn     (aresetn),\n'
        line += "  .A_TDATA     (absA_d[%d]),\n" % cmp_ex_pipe
        #line += '  .A_TDATA     (absA),\n'
        line += "  .A_TVALID    (a_vld_d[%d]),\n" % cmp_ex_pipe
        #line += '  .A_TVALID    (A_TVALID),\n'
        line += "  .B_TDATA     (\'b%s), // %s\n" % (upper, segments[seg][1])
        line += '  .B_TVALID    (1\'b1),\n'
        line += "  .R_TDATA     (leq_seg[%d]),\n" % seg
        line += '  .R_TVALID    ()\n'
        line += ');\n'
        line += '\n'

    if decode_pipe == 1: 
        line += 'always @(posedge aclk) begin\n'
    else: 
        line += 'always @(*) begin\n'
    line += '  case(leq_seg)\n'
    ones = ''
    for seg in range(num_segs + 1):
        line += "    \'b%s: rom_addr <= \'d%d;\n" % (ones.rjust(num_segs, '1'), seg)
        ones += '0'
    line += '    default: rom_addr <= \'b0;\n'
    line += '  endcase\n'
    line += 'end\n'
    line += '\n'

    rom_name = "%s_rom_%d" % (args.func, mac_count)
    line += "%s %s_inst\n" % (rom_name, rom_name)
    line += '(\n'
    line += '  .aclk    (aclk),\n'
    line += '  .addr    (rom_addr),\n' 
    line += "  .data    (rom%d_out)\n" % mac_count
    line += ');\n'
    line += '\n'

    line += "%s %s_inst\n" % (sqr_name, sqr_name)
    line += '(\n'
    line += '  .clk    (aclk),\n'
    line += '  .rst    (~aresetn),\n'
    line += "  .X      (absA_d[%d]),\n" % sqr_ex_pipe
    line += '  .R      (sqr_out)\n'
    line += ');\n'
    line += '\n'

    cur_pipe = start_pipe # 1 for first ROM
    #cur_pipe = cmp_pipe + decode_pipe + 1 # 1 for first ROM
    for d in reversed(range(mac_count)):
        line += "%s %s_inst_%d\n" % (mult_name, mult_name, d)
        line += '(\n'
        line += '  .clk    (aclk),\n'
        line += '  .rst    (~aresetn),\n'
        if d == 0:
            line += "  .X      (absA_d[%d]),\n" % cur_pipe
        else:
            line += "  .X      (sqr_out_d[%d]),\n" % (cur_pipe - start_pipe + reg_sqr) #TBD
        if d == mac_count - 1:
            # highest degree from ROM
            line += "  .Y      (rom%d_out),\n" % mac_count
        else:
            # otherwise from higher deg add output
            line += "  .Y      (add%d_out_d[%d]),\n" % (d + 1, reg_add)
        line += "  .R      (mult%d_out)\n" % d
        line += ');\n'
        line += '\n'
        cur_pipe = cur_pipe + mult_pipe + reg_mult

        rom_name = "%s_rom_%d" % (args.func, d)
        line += "%s %s_inst\n" % (rom_name, rom_name)
        line += '(\n'
        line += '  .aclk    (aclk),\n'
        line += "  .addr    (rom_addr_d[%d]),\n" %  (cur_pipe - start_pipe)
        #line += "  .addr    (rom_addr_d[%d]),\n" %  (cur_pipe - cmp_pipe - decode_pipe - 1)
        line += "  .data    (rom%d_out)\n" % d
        line += ');\n'
        line += '\n'

        line += "%s %s_inst_%d\n" % (add_name, add_name, d)
        line += '(\n'
        line += '  .clk    (aclk),\n'
        line += '  .rst    (~aresetn),\n'
        line += "  .X      (mult%d_out_d[%d]),\n" % (d, reg_mult)
        line += "  .Y      (rom%d_out),\n" % d
        line += "  .R      (add%d_out)\n" % d
        line += ');\n'
        line += '\n'
        cur_pipe = cur_pipe + add_pipe + reg_add
    line += '\n'
    return line

def gen_tanh(args, log_file):
    cmp_pipe = 0
    decode_pipe = 0
    if not args.odd and args.deg > 3 and args.wE == 8 and args.wF == 23:
        reg_mult = 1 # 1 to register the mult output
    else:
        reg_mult = 0 # 1 to register the mult output
    reg_add = 0 # 1 to register the add output
    reg_sqr = 0
    gen_cmp_leq(args.wE, args.wF, cmp_pipe)

    up_limit = "0x1p0-2^(-%d)" % (args.wF + 1)
    bound = find_upper_bound(args.wF, Mono.INC, ('tanh(x)', 'atanh(x)'), up_limit)
    exp_lo = (1 - 2**(args.wE - 1))
    exp_up = -1

    start = timer()
    (segments, failed) = search_func_seg(args, Mono.INC, ('tanh(x)', 'atanh(x)'), 
            ('0', bound), (exp_lo, exp_up), log_file)
    end = timer()
    log_file.write("elasped time for search_tanh_seg: %s\n" % str(end - start))

    if failed: return;
    num_segs = len(segments)
    log_file.write("segment count is %d\n" % num_segs)

    rom_content = list()
    coef = list()
    if args.odd: coef_count = int(math.ceil(args.deg/2)) + 1
    else: coef_count = args.deg + 1

    if not args.odd:
        # hardcoded 1st segment
        for d in range(args.deg + 1):
            if d == 1 or d == 2: coef.append('1.0')
            else: coef.append('0.0')
        rom_content.append(coef)
        for (lower, upper, err, poly) in segments[1:]:
            coef = horner2coef(poly[0])
            rom_content.append(coef)
    else:
        for (lower, upper, err, poly) in segments:
            coef = horner2coef(poly[0])
            rom_content.append(coef)
    coef = list()
    for d in range(coef_count):
        if d == 0: coef.append('1.0')
        else: coef.append('0.0')
    rom_content.append(coef)

    #(mult_name, mult_pipe) = create_mult_file(args)
    #(add_name, add_pipe) = create_add_file(args)
    mult_param = ['FPMult', "wE=%d" % args.wE, "wF=%d" % args.wF, "wFout=%d" % args.wF]
    add_param = ['FPAdd', "wE=%d" % args.wE, "wF=%d" % args.wF]
    (mult_name, mult_pipe) = create_flopoco_file(args, mult_param)
    (add_name, add_pipe) = create_flopoco_file(args, add_param)
    if args.odd:
        sqr_param = ['FPSquare', "wE=%d" % args.wE, "wF_in=%d" % args.wF, "wF_out=%d" % args.wF]
        (sqr_name, sqr_pipe) = create_flopoco_file(args, sqr_param)

    if args.odd:
        module_name = "fp_tanh_%d_%d_d%d_odd" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.ODD, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, reg_sqr, mult_name, mult_pipe, add_name, add_pipe, sqr_name, sqr_pipe)
    else:
        module_name = "fp_tanh_%d_%d_d%d" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.ODD, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, 0, mult_name, mult_pipe, add_name, add_pipe, '', 0)

    rom_addr_width = math.ceil(math.log(num_segs + 1, 2))
    line  = ''
    line += print_assign_rom(args, num_segs, rom_addr_width, rom_content)
    line += print_header_in1out1(args.wE, args.wF, module_name)
    if args.odd:
        line += print_horner_odd_pwr(args, params, segments)
    else:
        line += print_horner_func(args, params, segments)
    line += print_footer(module_name)
    file_out = open("./%s.v" % module_name, 'w')
    file_out.write(line);
    file_out.close()

def gen_sigmoid(args, log_file):
    cmp_pipe = 0
    decode_pipe = 0
    if not args.odd and args.deg > 3 and args.wE == 8 and args.wF == 23:
        reg_mult = 1 # 1 to register the mult output
    else:
        reg_mult = 0 # 1 to register the mult output
    reg_add = 0 # 1 to register the add output
    reg_sqr = 0
    gen_cmp_leq(args.wE, args.wF, cmp_pipe)

    lo_limit = "2^(%d)" % (1 - 2**(args.wE - 1))
    lo_bound = find_lower_bound(args.wF, Mono.INC, ('1/(1+exp(-x))', '-log(1/x-1)'), lo_limit)
    up_limit = "0x1p0 - 2^(-%d)" % (args.wF + 1)
    up_bound = find_upper_bound(args.wF, Mono.INC, ('1/(1+exp(-x))', '-log(1/x-1)'), up_limit)
    exp_lo = (1 - 2**(args.wE - 1))
    exp_up = -1

    start = timer()
    (segments, failed) = search_func_seg(args, Mono.INC, ('1/(1+exp(-x))', '-log(1/x-1)'), 
            (lo_bound, up_bound), (exp_lo, exp_up), log_file)
    end = timer()
    log_file.write("elasped time for search_tanh_seg: %s\n" % str(end - start))

    if failed: return;
    lo_bound_seg = ('', lo_bound, '', ('',''))
    segments.insert(0, lo_bound_seg) 
    num_segs = len(segments)
    log_file.write("segment count is %d\n" % num_segs)

    lo_limit_dec = run_sollya(lo_limit+';').split('\n')[0]
    rom_content = list()
    coef = list()
    if args.odd: coef_count = int(math.ceil(args.deg/2)) + 1
    else: coef_count = args.deg + 1

    for d in range(coef_count):
        if d == 0: coef.append(lo_limit_dec)
        else: coef.append('0.0')
    #eprint("1st row len(coef) %d" % len(coef))
    rom_content.append(coef)
    for (lower, upper, err, poly) in segments[1:]:
        coef = horner2coef(poly[0])
        #eprint("row len(coef) %d" % len(coef))
        rom_content.append(coef)
    coef = list()
    for d in range(coef_count):
        if d == 0: coef.append('1.0')
        else: coef.append('0.0')
    #eprint("last row len(coef) %d" % len(coef))
    rom_content.append(coef)

    #(mult_name, mult_pipe) = create_mult_file(args)
    #(add_name, add_pipe) = create_add_file(args)
    #(sqr_name, sqr_pipe) = create_sqr_file(args)
    mult_param = ['FPMult', "wE=%d" % args.wE, "wF=%d" % args.wF, "wFout=%d" % args.wF]
    add_param = ['FPAdd', "wE=%d" % args.wE, "wF=%d" % args.wF]
    (mult_name, mult_pipe) = create_flopoco_file(args, mult_param)
    (add_name, add_pipe) = create_flopoco_file(args, add_param)
    if args.odd:
        sqr_param = ['FPSquare', "wE=%d" % args.wE, "wF_in=%d" % args.wF, "wF_out=%d" % args.wF]
        (sqr_name, sqr_pipe) = create_flopoco_file(args, sqr_param)

    if (args.odd):
        module_name = "fp_sigmoid_%d_%d_d%d_odd" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.NO, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, reg_sqr, mult_name, mult_pipe, add_name, add_pipe, sqr_name, sqr_pipe)
    else:
        module_name = "fp_sigmoid_%d_%d_d%d" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.NO, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, 0, mult_name, mult_pipe, add_name, add_pipe, '', 0)

    rom_addr_width = math.ceil(math.log(num_segs + 1, 2))
    line  = ''
    line += print_assign_rom(args, num_segs, rom_addr_width, rom_content)
    line += print_header_in1out1(args.wE, args.wF, module_name)
    if args.odd:
        line += print_horner_odd_pwr(args, params, segments)
    else:
        line += print_horner_func(args, params, segments)
    line += print_footer(module_name)
    file_out = open("./%s.v" % module_name, 'w')
    file_out.write(line);
    file_out.close()

def gen_dtanh(args, log_file):
    cmp_pipe = 0
    decode_pipe = 0
    if not args.odd and args.deg > 3 and args.wE == 8 and args.wF == 23:
        reg_mult = 1 # 1 to register the mult output
    else:
        reg_mult = 0 # 1 to register the mult output
    reg_add = 0 # 1 to register the add output
    reg_sqr = 0
    gen_cmp_leq(args.wE, args.wF, cmp_pipe)

    lo_limit = "2^(%d)" % (1 - 2**(args.wE - 1))
    lo_bound = '0'
    up_bound = find_upper_bound(args.wF, Mono.DEC, ('1-tanh(x)^2', 'atanh((1-x)^(1/2))'), lo_limit)
    exp_lo = (1 - 2**(args.wE - 1))
    exp_up = -1
    eprint("up_bound %s" % up_bound)

    start = timer()
    (segments, failed) = search_func_seg(args, Mono.DEC, ('1-tanh(x)^2', 'atanh((1-x)^(1/2))'), 
            (lo_bound, up_bound), (exp_lo, exp_up), log_file)
    end = timer()
    log_file.write("elasped time for search_tanh_seg: %s\n" % str(end - start))

    if failed: return;
    num_segs = len(segments)
    log_file.write("segment count is %d\n" % num_segs)

    lo_limit_dec = run_sollya(lo_limit+';').split('\n')[0]
    rom_content = list()
    coef = list()
    if args.odd: coef_count = int(math.ceil(args.deg/2)) + 1
    else: coef_count = args.deg + 1

    for (lower, upper, err, poly) in segments:
        coef = horner2coef(poly[0])
        rom_content.append(coef)
    coef = list()
    for d in range(coef_count):
        if d == 0: coef.append(lo_limit_dec)
        else: coef.append('0.0')
    rom_content.append(coef)

    mult_param = ['FPMult', "wE=%d" % args.wE, "wF=%d" % args.wF, "wFout=%d" % args.wF]
    add_param = ['FPAdd', "wE=%d" % args.wE, "wF=%d" % args.wF]
    (mult_name, mult_pipe) = create_flopoco_file(args, mult_param)
    (add_name, add_pipe) = create_flopoco_file(args, add_param)
    if args.odd:
        sqr_param = ['FPSquare', "wE=%d" % args.wE, "wF_in=%d" % args.wF, "wF_out=%d" % args.wF]
        (sqr_name, sqr_pipe) = create_flopoco_file(args, sqr_param)

    if (args.odd):
        module_name = "fp_dtanh_%d_%d_d%d_odd" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.EVEN, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, reg_sqr, mult_name, mult_pipe, add_name, add_pipe, sqr_name, sqr_pipe)
    else:
        module_name = "fp_dtanh_%d_%d_d%d" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.EVEN, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, 0, mult_name, mult_pipe, add_name, add_pipe, '', 0)

    rom_addr_width = math.ceil(math.log(num_segs + 1, 2))
    line  = ''
    line += print_assign_rom(args, num_segs, rom_addr_width, rom_content)
    line += print_header_in1out1(args.wE, args.wF, module_name)
    if args.odd:
        line += print_horner_odd_pwr(args, params, segments)
    else:
        line += print_horner_func(args, params, segments)
    line += print_footer(module_name)
    file_out = open("./%s.v" % module_name, 'w')
    file_out.write(line);
    file_out.close()

def gen_dsigmoid(args, log_file):
    cmp_pipe = 0
    decode_pipe = 0
    if not args.odd and args.deg > 3 and args.wE == 8 and args.wF == 23:
        reg_mult = 1 # 1 to register the mult output
    else:
        reg_mult = 0 # 1 to register the mult output
    reg_add = 0 # 1 to register the add output
    reg_sqr = 0
    gen_cmp_leq(args.wE, args.wF, cmp_pipe)

    lo_limit = "2^(%d)" % (1 - 2**(args.wE - 1))
    lo_bound = '0'
    up_bound = find_upper_bound(args.wF, Mono.DEC, ('exp(-x)/(exp(-x)+1)^2', 'log((x*(-2) + (1 + x*(-4))^(1/2) + 1)/(x*2))'), lo_limit)
    exp_lo = (1 - 2**(args.wE - 1))
    exp_up = -3
    eprint("up_bound %s" % up_bound)

    start = timer()
    (segments, failed) = search_func_seg(args, Mono.DEC, ('exp(-x)/(exp(-x)+1)^2', 'log((x*(-2) + (1 + x*(-4))^(1/2) + 1)/(x*2))'), 
            (lo_bound, up_bound), (exp_lo, exp_up), log_file)
    end = timer()
    log_file.write("elasped time for search_tanh_seg: %s\n" % str(end - start))

    if failed: return;
    num_segs = len(segments)
    log_file.write("segment count is %d\n" % num_segs)

    lo_limit_dec = run_sollya(lo_limit+';').split('\n')[0]
    rom_content = list()
    coef = list()
    if args.odd: coef_count = int(math.ceil(args.deg/2)) + 1
    else: coef_count = args.deg + 1

    for (lower, upper, err, poly) in segments:
        coef = horner2coef(poly[0])
        rom_content.append(coef)
    coef = list()
    for d in range(coef_count):
        if d == 0: coef.append(lo_limit_dec)
        else: coef.append('0.0')
    rom_content.append(coef)

    mult_param = ['FPMult', "wE=%d" % args.wE, "wF=%d" % args.wF, "wFout=%d" % args.wF]
    add_param = ['FPAdd', "wE=%d" % args.wE, "wF=%d" % args.wF]
    (mult_name, mult_pipe) = create_flopoco_file(args, mult_param)
    (add_name, add_pipe) = create_flopoco_file(args, add_param)
    if args.odd:
        sqr_param = ['FPSquare', "wE=%d" % args.wE, "wF_in=%d" % args.wF, "wF_out=%d" % args.wF]
        (sqr_name, sqr_pipe) = create_flopoco_file(args, sqr_param)

    if (args.odd):
        module_name = "fp_dsigmoid_%d_%d_d%d_odd" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.EVEN, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, reg_sqr, mult_name, mult_pipe, add_name, add_pipe, sqr_name, sqr_pipe)
    else:
        module_name = "fp_dsigmoid_%d_%d_d%d" % (args.wE, args.wF, args.deg)
        params = horner_param(Symmetry.EVEN, num_segs, cmp_pipe, decode_pipe, 
                reg_mult, reg_add, 0, mult_name, mult_pipe, add_name, add_pipe, '', 0)

    rom_addr_width = math.ceil(math.log(num_segs + 1, 2))
    line  = ''
    line += print_assign_rom(args, num_segs, rom_addr_width, rom_content)
    line += print_header_in1out1(args.wE, args.wF, module_name)
    if args.odd:
        line += print_horner_odd_pwr(args, params, segments)
    else:
        line += print_horner_func(args, params, segments)
    line += print_footer(module_name)
    file_out = open("./%s.v" % module_name, 'w')
    file_out.write(line);
    file_out.close()

parser = argparse.ArgumentParser(
        description = 'NnCore: a Neural-Network arithmetic CORE generator')

parser.add_argument('func', 
        choices = ['cmp_cndt_code', 'cmp_lss', 'cmp_leq', 'relu', 'relu6', 
            'tanh', 'sigmoid', 'd_tanh', 'd_sigmoid'], 
        help = 'the target function to be generated')

parser.add_argument('--wE', type = int, choices = range(4, 17), required = True,
        help = 'Exponent width (range 4-16)', metavar = 'exp_width')

parser.add_argument('--wF', type = int, choices = range(4, 65), required = True,
        help = 'Mantissa width (range 4-64)', metavar = 'man_width')

parser.add_argument('-p', '--pipe', default = 0, type = int, 
        choices = range(0, 2), required = False, 
        help = 'Pipeline depth, 0 for combinational, maximum varies for each '
        'function', metavar = 'depth')

parser.add_argument('-c', '--const', default = 6.0, type = float, 
        required = False, help = 'Constant compared in ReLU6 (default: 6.0)')

parser.add_argument('-d', '--deg', default = 3, type = int, 
        required = False, help = 'Maximum power of the minimax polynomial (default: 3)')

parser.add_argument('--odd', action='store_true', 
        required = False, help = 'To only use odd powers for the minimax polynomial (default: False)')

parser.add_argument('--no_mono', action='store_true', 
        required = False, help = 'Disable monotonicity check, debug feature (default: False)')

parser.add_argument('--approx_err', default = 1.0, type = float, 
        required = False, help = 'The accepted approx. error in ULP (default: 1.0)')

args = parser.parse_args()

#dict = {
#        'ReLU': gen_relu,
#        'ReLU6': ,
#        'Tanh': ,
#        'Sigmoid': ,
#        'ELU': ,
#        }
#
#dict.get(args.func, )

if args.func == 'relu':
    gen_relu(args.wE, args.wF)
elif args.func == 'relu6':
    gen_relu6(args.wE, args.wF, args.pipe, args.const)
elif args.func == 'cmp_cndt_code':
    gen_cmp_cndt_code(args.wE, args.wF, args.pipe)
elif args.func == 'cmp_lss':
    gen_cmp_lss(args.wE, args.wF, args.pipe)
elif args.func == 'cmp_leq':
    gen_cmp_leq(args.wE, args.wF, args.pipe)
elif args.func == 'sigmoid':
    if args.odd:
        log_file = open("sigmoid_e%df%d_d%d_odd.log" % (args.wE, args.wF, args.deg), 'w')
    else:
        log_file = open("sigmoid_e%df%d_d%d.log" % (args.wE, args.wF, args.deg), 'w')

    log_file.write("Options used: wE=%d, wF=%d, deg=%d, odd=%s, " \
            "approx_err=%f, no_mono=%s\n" % (args.wE, args.wF, args.deg, args.odd, 
                args.approx_err, args.no_mono))
    gen_sigmoid(args, log_file)
    log_file.close()

elif args.func == 'tanh':
    if args.odd:
        log_file = open("tanh_e%df%d_d%d_odd.log" % (args.wE, args.wF, args.deg), 'w')
    else:
        log_file = open("tanh_e%df%d_d%d.log" % (args.wE, args.wF, args.deg), 'w')

    log_file.write("Options used: wE=%d, wF=%d, deg=%d, odd=%s, " \
            "approx_err=%f, no_mono=%s\n" % (args.wE, args.wF, args.deg, args.odd, 
                args.approx_err, args.no_mono))
    gen_tanh(args, log_file)
    log_file.close()

elif args.func == 'd_tanh':
    if args.odd:
        log_file = open("dtanh_e%df%d_d%d_odd.log" % (args.wE, args.wF, args.deg), 'w')
    else:
        log_file = open("dtanh_e%df%d_d%d.log" % (args.wE, args.wF, args.deg), 'w')
    gen_dtanh(args, log_file)
    log_file.close()

elif args.func == 'd_sigmoid':
    if args.odd:
        log_file = open("dsigmoid_e%df%d_d%d_odd.log" % (args.wE, args.wF, args.deg), 'w')
    else:
        log_file = open("dsigmoid_e%df%d_d%d.log" % (args.wE, args.wF, args.deg), 'w')
    gen_dsigmoid(args, log_file)
    log_file.close()

else:
    print('The target function ' + args.func + ' is not implemented yet.');


