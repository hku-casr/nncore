#!/usr/bin/python3
from __future__ import print_function # for eprint
from collections import deque, namedtuple
from itertools import islice
from math import ceil, log2
from timeit import default_timer as timer
import argparse
import logging
import os
import pickle
import re
import sollya
import subprocess
import sys # for eprint
import time
__author__ = 'sammhho'

def enum(**enums):
    return type('Enum', (), enums)

Mono = enum(INC=0, DEC=1)

Parity = enum(NO=0, ODD=1, EVEN=2)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def nothing(args):
    print("Target function %s not implemented yet." % args.func)

def run_fp2bin(command):
    cmd_list = ['fp2bin']
    cmd_list.extend(command)
    fp2bin = subprocess.Popen(cmd_list, shell=False, stdin=subprocess.PIPE, 
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
                              universal_newlines=True)
    return fp2bin.communicate()[0]

class FuncSpec:
    def __init__(self, funcTup, args):
        (fdef, invf, mono, par) = funcTup
        self.fdef = fdef
        self.invf = invf
        self.mono = mono
        self.par = par
        self.we = args.we
        self.wf = args.wf
        self.deg = args.deg
        self.exp_lo = 1 - 2**(args.we-1)
        self.exp_up = 2**(args.we-1)
        self.y_up = sollya.eval("0x1p%d-0x1p%d;" % 
                                (self.exp_up+1, self.exp_up-args.wf))
        self.no_scale = True
    def setTransDict(self, transDict):
        self.transDict = transDict

segNameTup = namedtuple('Segment', ['x_lo', 'x_up', 'x_e', 'y_e', 'x_sgn', 
                                    'y_sgn', 'y_flag', 'x_lo_y', 'x_up_y'])
segNameTup.__new__.__defaults__ = ('', ) * len(segNameTup._fields)

class Segment(segNameTup):
    def __str__(self):
        return (' {0.x_lo:>16}, {0.x_up:>16}, {0.x_sgn:>5}, {0.x_e:>4},'
                ' {0.x_lo_y:>16}, {0.x_up_y:>16},'
                ' {0.y_flag:>6}, {0.y_sgn:>5}, {0.y_e:>4},').format(self)
    def y_is_const(self):
        return (self.x_lo_y==self.x_up_y and self.x_lo_y)
    def y_equal(self, seg):
        return (self.y_is_const() and seg.y_is_const() and 
                self.x_lo_y==seg.x_lo_y)
    def toPSeg(self, p):
        return PolySegment._make(self + p)
    def mergeright(self, s):
        kwargs = {'x_up':s.x_up, 'x_lo_y':self.x_lo_y, 'x_up_y':s.x_up_y}
        if (not self.x_sgn == s.x_sgn): kwargs['x_sgn'] = ''
        if (not self.x_e == s.x_e): kwargs['x_e'] = ''
        if (not self.y_e == s.y_e): kwargs['y_e'] = ''
        mrg_s = self._replace(**kwargs)
        logging.debug(' mergeright [%s;%s] and [%s;%s]', self.x_lo, self.x_up, s.x_lo, s.x_up)
        return mrg_s

Poly = namedtuple('Poly', ['f_str', 'poly', 'u_err', 'y_err', 'p_scale'])
Poly.__new__.__defaults__ = ('', ) * len(Poly._fields)

class PolySegment(namedtuple('PolySegment', Segment._fields + Poly._fields)):
    def __str__(self):
        #logging.info((' {0:<15} {1:<15} {4}, {2:>4}, {6}, {5}, {3:>4}, '
        #             '{9:<15} {10:<15} {8}, {7}').format(*PolySegment._fields))
        return (' {0.x_lo:>16}, {0.x_up:>16}, {0.x_sgn:>5}, {0.x_e:>4},'
                ' {0.x_lo_y:>16}, {0.x_up_y:>16},'
                ' {0.y_flag:>6}, {0.y_sgn:>5}, {0.y_e:>4}, {0.u_err:>16},'
                #'{0.y_flag}, {0.y_sgn:1}, {0.y_e:>4}, {0.u_err:<16} {0.y_err:<16} '
                '{0.p_scale:>2}, p = {0.poly}, f = {0.f_str},').format(self)
    def testPoints(self, spec):
        def printPoint(x):
            y = sollya.yrnd(spec, x)
            xdec = sollya.eval('display=decimal!;{0};'.format(x))
            ydec = sollya.eval('display=decimal!;{0};'.format(y))
            xbin = run_fp2bin([str(spec.we), str(spec.wf), xdec]).split('\n')[0]
            ybin = run_fp2bin([str(spec.we), str(spec.wf), ydec]).split('\n')[0]
            return [xbin, ybin, x, y]
        fstr = '{0}, {1}, {2}, {3}\n'
        teststr = fstr.format(*printPoint(self.x_lo))
        teststr += fstr.format(*printPoint(self.x_up))
        return teststr
    def toSeg(self):
        return Segment._make(self[:len(Segment._fields)])
    def mergeright(self, s):
        kwargs = {'x_up':s.x_up, 'x_lo_y':self.x_lo_y, 'x_up_y':s.x_up_y}
        if (not self.x_sgn == s.x_sgn): kwargs['x_sgn'] = ''
        if (not self.x_e == s.x_e): kwargs['x_e'] = ''
        if (not self.y_e == s.y_e): kwargs['y_e'] = ''
        # Below is unique to pseg
        if (sollya.greater(self.u_err, s.u_err)):
            kwargs['u_err'] = self.u_err
        else:
            kwargs['u_err'] = s.u_err
        mrg_s = self._replace(**kwargs)
        logging.debug(' mergeright [%s;%s] and [%s;%s]', self.x_lo, self.x_up, s.x_lo, s.x_up)
        return mrg_s

def create_seg_by_binade(spec):
    seglist1 = list()
    seglist1.append(Segment(x_lo='NaN', x_lo_y=sollya.yrnd(spec, 'NaN')))
    seglist1.append(Segment(x_lo='-infty', x_sgn='1', 
                            x_lo_y=sollya.yrnd(spec, '-infty')))
    seglist1.append(Segment(x_lo='-0', x_sgn='1', x_lo_y=sollya.yrnd(spec, '-0')))
    seglist1.append(Segment(x_lo='0', x_sgn='0', x_lo_y=sollya.yrnd(spec, '0')))
    seglist1.append(Segment(x_lo='infty', x_sgn='0', 
                            x_lo_y=sollya.yrnd(spec, 'infty')))
    if spec.par == Parity.NO:
        for exp in range(spec.exp_up, spec.exp_lo-1, -1):
            lo_str = '-0x1p%d+0x1p%d' % (exp+1, exp-spec.wf)
            seglist1.append(Segment(x_lo=sollya.eval('%s;'%lo_str), 
                                    x_up='-0x1p%d'%exp, 
                                    x_sgn='1', x_e=str(exp),
                                    x_lo_y=sollya.yrnd(spec, lo_str),
                                    x_up_y=sollya.yrnd(spec, '-0x1p%d'%exp)))
    for exp in range(spec.exp_lo, spec.exp_up+1):
        up_str = '0x1p%d-0x1p%d' % (exp+1, exp-spec.wf)
        seglist1.append(Segment(x_lo='0x1p%d'%exp, 
                                x_up=sollya.eval('%s;'%up_str), 
                                x_sgn='0', x_e=str(exp),
                                x_lo_y=sollya.yrnd(spec, '0x1p%d'%exp),
                                x_up_y=sollya.yrnd(spec, up_str)))
    logging.info(' ------------\n')
    logging.info(' create_seg_by_binade: %d', len(seglist1))
    logging.info(Segment._make(Segment._fields))
    for s in seglist1:
        logging.info(s)
    logging.info(' create_seg_by_binade end\n')
    return seglist1

def binary_search(spec, x_lo, x_up, t_bin):
    x_mid = sollya.midpoint(spec, x_lo, x_up)
    mid_bin = sollya.bin(sollya.yrnd(spec, x_mid))
    if x_mid == x_lo: 
        return x_lo
    elif mid_bin == t_bin:
        return binary_search(spec, x_mid, x_up, t_bin)
    else:
        return binary_search(spec, x_lo, x_mid, t_bin)

def split_seg_by_range_binade(spec, seglist_in):
    seglist_out = list()
    flag = {'0': '00', 'infty': '10', '-infty': '10', 'NaN': '11',}
    for s in seglist_in:
        cur_s = s
        while (cur_s.x_up and not sollya.bin_eq(cur_s.x_lo_y, cur_s.x_up_y)):
            new_x_up = binary_search(spec, cur_s.x_lo, cur_s.x_up, 
                                     sollya.bin(cur_s.x_lo_y))
            new_x_up_y = sollya.yrnd(spec, new_x_up)
            (y_sgn, y_e) = sollya.bin(new_x_up_y)
            new_s = cur_s._replace(x_up=new_x_up, x_up_y=new_x_up_y, 
                                   y_sgn=y_sgn, y_e=y_e, 
                                   y_flag=flag.get(cur_s.x_lo_y, '01'))
            seglist_out.append(new_s)
            nxt_x_lo = sollya.add_ulp(spec, new_x_up)
            nxt_x_lo_y = sollya.yrnd(spec, nxt_x_lo)
            cur_s = cur_s._replace(x_lo=nxt_x_lo, x_lo_y=nxt_x_lo_y)
        (y_sgn, y_e) = sollya.bin(cur_s.x_lo_y)
        cur_s = cur_s._replace(y_sgn=y_sgn, y_e=y_e, 
                               y_flag=flag.get(cur_s.x_lo_y, '01'))
        seglist_out.append(cur_s)
    logging.info(' ------------\n')
    logging.info(' split_seg_by_range_binade: %d', len(seglist_out))
    logging.info(Segment._make(Segment._fields))
    for s in seglist_out:
        logging.info(s)
    logging.info(' split_seg_by_range_binade end\n')
    return seglist_out

# deprecated
def merge_seg_by_y(spec, seglist_in):
    last_j = -1
    seglist_out = list()
    for i, cur_s in enumerate(seglist_in):
        if (i < len(seglist_in)-1 and i > last_j):
            nxt_s = seglist_in[i+1]
            if (cur_s.y_equal(nxt_s)):
                if (not cur_s.x_sgn == nxt_s.x_sgn): mrg_sgn = ''
                else: mrg_sgn = cur_s.x_sgn
                mrg_s = cur_s._replace(x_up=nxt_s.x_up, x_sgn=mrg_sgn, x_e='')
                last_j = i+1
                for j in range(i+2, len(seglist_in)):
                    j_s = seglist_in[j]
                    if (cur_s.y_equal(j_s)):
                        if (not cur_s.x_sgn == j_s.x_sgn): mrg_sgn = ''
                        else: mrg_sgn = cur_s.x_sgn
                        mrg_s = mrg_s._replace(x_up=j_s.x_up, x_sgn=mrg_sgn)
                        last_j = j
                    else: break
                seglist_out.append(mrg_s)
            else: 
                seglist_out.append(cur_s)
        elif (i == len(seglist_in)-1 and i > last_j):
            seglist_out.append(cur_s)
    logging.info(' ------------\n')
    logging.info(' merge_seg_by_y: %d', len(seglist_out))
    logging.info(Segment._make(Segment._fields))
    for s in seglist_out:
        logging.info(s)
    logging.info(' merge_seg_by_y end\n')
    return seglist_out

def y_cond(s1, s2):
    return s1.y_equal(s2)

def poly_cond(ps1, ps2):
    return (ps1.x_up and ps2.x_up and
            ps1.y_flag == '01' and 
            ps2.y_flag == '01' and
            ps1.y_sgn == ps2.y_sgn and
            ps1.y_e == ps2.y_e and
            ps1.p_scale == ps2.p_scale and
            ps1.poly == ps2.poly)

def mirror_cond(ps1, ps2):
    return (ps1.x_up and ps2.x_up and
            ps1.y_flag=='01' and 
            ps2.y_flag=='01' and
            ps1.y_sgn == ps2.y_sgn and
            ps1.x_e == ps1.y_e and
            ps2.x_e == ps2.y_e and
            ps1.p_scale == '' and
            ps2.p_scale == '' and
            ps1.poly == ps2.poly and 
            ps1.poly=='x')

def merge_seg_by_cond(spec, seglist_in, cond):
    last_j = -1
    seglist_out = list()
    for i, cur_s in enumerate(seglist_in):
        if (i < len(seglist_in)-1 and i > last_j):
            nxt_s = seglist_in[i+1]
            if cond(cur_s, nxt_s):
                mrg_s = cur_s.mergeright(nxt_s)
                last_j = i+1
                for j in range(i+2, len(seglist_in)):
                    j_s = seglist_in[j]
                    if cond(cur_s, j_s):
                        mrg_s = mrg_s.mergeright(j_s)
                        last_j = j
                    else: break
                seglist_out.append(mrg_s)
            else: 
                seglist_out.append(cur_s)
        elif (i == len(seglist_in)-1 and i > last_j):
            seglist_out.append(cur_s)
    logging.info(' ------------\n')
    logging.info(' merge_seg_by_cond: %s', cond.__name__)
    logging.info(' merge_seg_by_cond: %d', len(seglist_out))
    logging.info(PolySegment._make(PolySegment._fields))
    for s in seglist_out:
        logging.info(s)
    logging.info(' merge_seg_by_cond end\n')
    return seglist_out

# deprecated
def gdpoly(poly, exp_bound):
    pattern = re.compile('p(-?\d+)')
    for m in pattern.finditer(poly):
        exp = m.group(1)
        if int(exp) > exp_bound:
            logging.debug(' gdpoly return false: %s', poly)
            return False
    return True

def split_seg_by_real_poly(spec, seglist_in, err_bound):
    seglist_out = list()
    segdeque = deque(seglist_in)
    #segdeque = deque([(s, False) for s in seglist_in])
    while (len(segdeque) > 0):
        cur_s = segdeque.popleft()
        #cur_s, skipgdpoly = segdeque.popleft()
        # skip for special input, merged const seg, or single points
        if ((not cur_s.x_up) or (not cur_s.x_e) or (cur_s.x_lo == cur_s.x_up)):
            p = Poly()
            seglist_out.append(cur_s.toPSeg(p))
            continue
        fstr = sollya.transform_func(spec, cur_s)
        (poly, err) = sollya.approx(spec, fstr, cur_s)
        err_met = sollya.lessOrEqual(err, err_bound)
        if err_met:
        #if (sollya.lessOrEqual(err, err_bound) and (gdpoly(poly, 4) or skipgdpoly)):
            p = Poly(f_str=fstr, poly=poly, u_err=err)
            seglist_out.append(cur_s.toPSeg(p))
        else:
            x_mid = sollya.midpoint(spec, cur_s.x_lo, cur_s.x_up)
            segdeque.appendleft(cur_s._replace(x_lo=sollya.add_ulp(spec, x_mid), x_lo_y=''))
            segdeque.appendleft(cur_s._replace(x_up=x_mid, x_up_y=''))
            #segdeque.appendleft((cur_s._replace(x_lo=sollya.add_ulp(spec, x_mid), x_lo_y=''), err_met))
            #segdeque.appendleft((cur_s._replace(x_up=x_mid, x_up_y=''), err_met))
            logging.debug(' real splitting [%s;%s] at %s', cur_s.x_lo, cur_s.x_up, x_mid)
    logging.info(' ------------\n')
    logging.info(' split_seg_by_real_poly: %d', len(seglist_out))
    logging.info(PolySegment._make(PolySegment._fields))
    for s in seglist_out:
        logging.info(s)
    logging.info(' split_seg_by_real_poly end\n')
    return seglist_out

def poly_max_exp(spec, poly):
    pattern = re.compile('p(-?\d+)')
    max_e = -(spec.wf)
    for m in pattern.finditer(poly):
        exp = int(m.group(1))
        if exp > max_e: max_e = exp
    return max_e

def split_seg_by_fix_poly(spec, seglist_in, err_bound):
    seglist_out = list()
    segdeque = deque(seglist_in)
    while (len(segdeque) > 0):
        cur_s = segdeque.popleft()
        # skip for special input, but not merged const seg, and single points
        if not cur_s.x_up:
            # should add y_flag
            seglist_out.append(cur_s)
            continue
        err_met = False
        for d in range(1, spec.deg+1):
            # make sure there is x_lo_y
            if (cur_s.x_lo == cur_s.x_up):
                x_lo_y = sollya.yrnd(spec, cur_s.x_lo)
                cur_s = cur_s._replace(x_lo_y=x_lo_y)
            # fixapprox2 needs to handle merged const seg and single points
            (poly, err) = sollya.fixapprox2(spec, cur_s, d)
            max_e = poly_max_exp(spec, poly)
            logging.debug(' fixapprox returned err %s', err)
            logging.debug(' fixapprox returned poly %s', poly)
            logging.debug(' fixapprox returned max_e %d', max_e)
            if sollya.lessOrEqual(err, err_bound):
                def scale_tranfunc_and_rerun_fixapprox():
                    nonlocal err_met
                    nonlocal max_e
                    if (max_e < 0): # set to <= for transDict1 !!!, originally < 0 for transDict0
                        err_met = True
                        seglist_out.append(cur_s._replace(poly=poly, u_err=err))
                    else:
                        logging.debug(' scaling poly %s', poly)
                        p_scale = max_e + 1
                        #p_scale = max_e//2
                        #fstr = sollya.eval("fs := 2^(%d)*(%s); fs;" % (-p_scale, cur_s.f_str))
                        #logging.debug(' scaling fstr %s', fstr)
                        new_s = cur_s._replace(p_scale=p_scale)
                        #new_s = cur_s._replace(f_str=fstr, p_scale=p_scale)
                        (new_poly, new_err) = sollya.fixapprox2(spec, new_s, d)
                        new_err_met = sollya.lessOrEqual(new_err, err_bound)
                        max_e = poly_max_exp(spec, new_poly)
                        logging.debug(' rerun fixapprox scaled new_err: %s', new_err)
                        logging.debug(' rerun fixapprox scaled poly: %s', new_poly)
                        logging.debug(' rerun fixapprox max_e: %d', max_e)
                        if (new_err_met and max_e < 0): # set to <= same as above !!!
                            err_met = True
                            seglist_out.append(new_s._replace(poly=new_poly, u_err=new_err))
                            logging.debug(' scaling poly success')
                        else:
                            logging.debug(' scaling poly failed')
                def scale_p_but_dont_rerun():
                    nonlocal err_met
                    err_met = True
                    if (max_e < 0):
                        seglist_out.append(cur_s._replace(poly=poly, u_err=err))
                    else:
                        logging.debug(' scaling poly %s', poly)
                        p_scale = max_e + 1
                        fstr = sollya.eval("fs := 2^(%d)*(%s); fs;" % (-p_scale, poly))
                        logging.debug(' scaled poly %s', fstr)
                        seglist_out.append(cur_s._replace(poly=fstr, u_err=err, p_scale=p_scale))
                def dont_scale():
                    nonlocal err_met
                    err_met = True
                    if (max_e < 0):
                        seglist_out.append(cur_s._replace(poly=poly, u_err=err))
                    else:
                        seglist_out.append(cur_s._replace(poly=poly, u_err=err, p_scale=max_e))
                #scale_tranfunc_and_rerun_fixapprox()
                #scale_p_but_dont_rerun()
                dont_scale()
                if err_met: break
        if not err_met:
            x_mid = sollya.midpoint(spec, cur_s.x_lo, cur_s.x_up)
            segdeque.appendleft(cur_s._replace(x_lo=sollya.add_ulp(spec, x_mid), x_lo_y=''))
            segdeque.appendleft(cur_s._replace(x_up=x_mid, x_up_y=''))
            logging.debug(' fixed splitting [%s;%s] at %s', cur_s.x_lo, cur_s.x_up, x_mid)
    logging.info(' ------------\n')
    logging.info(' split_seg_by_fix_poly: %d', len(seglist_out))
    logging.info(PolySegment._make(PolySegment._fields))
    for s in seglist_out:
        logging.info(s)
    logging.info(' split_seg_by_fix_poly end\n')
    return seglist_out

def outlier_recur(spec, s, scale_bound):
    x_mid = sollya.midpoint(spec, s.x_lo, s.x_up)
    mlist = list()
    mlist.append(s._replace(x_up=x_mid, x_up_y='', p_scale=''))
    mlist.append(s._replace(x_lo=sollya.add_ulp(spec, x_mid), x_lo_y='', p_scale=''))
    fixlist = split_seg_by_fix_poly(spec, mlist, '0x1p-%s'%(spec.wf+2))
    retlist = list()
    for fs in fixlist:
        if fs.p_scale and fs.p_scale > scale_bound:
            retlist.extend(outlier_recur(spec, fs, scale_bound))
        else:
            retlist.append(fs)
    return retlist

def split_outliers(spec, seglist_in):
    retlist = list()
    slist = seglist_in
    n = len(slist)
    max_scale = max(s.p_scale for s in slist if s.p_scale)
    avg_scale = (sum(s.p_scale+2 if s.p_scale else 1 for s in slist) / n)
    logging.info(' max p_scale is %d', max_scale)
    logging.info(' avg p_scale is %d', avg_scale)
    for s in slist:
        if s.p_scale and s.p_scale > avg_scale * 2:
            retlist.extend(outlier_recur(spec, s, avg_scale * 2))
        else:
            retlist.append(s)
    logging.info(' ------------\n')
    logging.info(' split_outliers: %d', len(retlist))
    logging.info(PolySegment._make(PolySegment._fields))
    for s in retlist:
        logging.info(s)
    logging.info(' split_outliers end\n')
    return retlist 

def print_segmentor_rom(spec, logfname, seglist_in):
    slist = seglist_in[5:]
    n = len(slist)
    k = ceil(log2(n))
    with open(os.path.join(logfname, logfname+'.h'), 'w') as f:
        romstr = '\n#ifndef {0}\n#define {0}\n\n'.format(logfname.upper().replace('-', '_')+'_H')
        romstr += '#include "ap_int.h"\n'
        romstr += '#include "ap_cint.h"\n\n'
        romstr += '#include "%s-coef.h"\n\n' % logfname
        romstr += '#define ADDRW %d\n' % k
        romstr += '#define WE %d\n' % spec.we
        romstr += '#define WF %d\n' % spec.wf
        romstr += '#define WFLO (3+WE+WF)\n\n'
        f.write(romstr)
        for l in range(0, k):
            romstr = 'static ap_uint<{0}> const b{1}[{2}] = {{'
            for i in range(0, 2**l):
                addr = (2**(k-l-1))*(2*i+1)
                if l > 0 and i > 0 and addr < n:
                    romstr += ','
                if addr < n:
                    #romstr += slist[addr].x_up
                    decstr = sollya.eval("display=decimal!;%s;" % slist[addr].x_lo)
                    args = [str(spec.we), str(spec.wf), decstr]
                    # Below slice off the starting '01'
                    romstr += '0b' + run_fp2bin(args).split('\n')[0][2:]
                else: 
                    romsize = i
                    break
                if (i+1) % 8 == 0:
                    romstr += '\n'
                romsize = i+1
            romstr += '}};\n'
            f.write(romstr.format(1+spec.we+spec.wf, l, romsize))
        segstr = (
            '\n'
            'template<int WT> bool geq(ap_uint<WT> &a, ap_uint<WT> &b) {\n'
            '   ap_uint<4> excpt = (a.range(WT - 1, WT - 2), b.range(WT - 1, WT - 2));\n'
            '   ap_uint<2> sign = (a[WT - 3], b[WT - 3]);\n'
            '   ap_uint<WT - 3> asig = a.range(WT - 4, 0);\n'
            '   ap_uint<WT - 3> bsig = b.range(WT - 4, 0);\n'
            '   switch (excpt) {\n'
            '   case 0b0000: // 0, 0\n'
            '       return true;\n'
            '   case 0b0101:\n'
            '       return sign == 0b01 || (sign==0b00 && asig >= bsig) || (sign==0b11 && asig <= bsig);\n'
            '   case 0b0001: // 0, norm\n'
            '   case 0b0010: // 0, inf\n'
            '   case 0b0110: // norm, inf\n'
            '       return sign[0];\n'
            '   case 0b0100: // norm, 0\n'
            '   case 0b1000: // inf, 0\n'
            '   case 0b1001: // inf, norm\n'
            '       return !sign[1];\n'
            '   case 0b1010: // inf, inf\n'
            '       return sign != 0b10;\n'
            '   case 0b0011:\n'
            '   case 0b0111:\n'
            '   case 0b1011:\n'
            '   case 0b1100:\n'
            '   case 0b1101:\n'
            '   case 0b1110:\n'
            '   case 0b1111:\n'
            '   default: // NaN\n'
            '       return false;\n'
            '   }\n'
            '}\n\n')
        f.write(segstr)
        segstr = (
            'static ap_uint<ADDRW> segmentor(ap_uint<WFLO> &x) {\n'
            '#pragma HLS PIPELINE II=1\n'
            '    ap_uint<ADDRW> addr;\n'
            '    ap_uint<2> excpt = 0b01;\n')
        segstr += '    bool l0'
        for l in range(1, k):
            segstr += ", l%d" % l
        segstr += ';\n'
        segstr += '    ap_uint<WFLO> cb0'
        for l in range(1, k):
            segstr += ", cb%d" % l
        segstr += ';\n'
        segstr += '    uint16 a1'
        for l in range(2, k):
            segstr += ", a%d" % l
        segstr += ';\n'
        segstr += '    cb0 = (excpt, b0[0]);\n'
        segstr += '    l0 = geq(x, cb0);\n'

        segstr += '    a1 = l0;\n'
        segstr += '    cb1 = (excpt, b1[a1]);\n'
        if ((n-1)//2**(k-1-1) % 2 == 0):
            segstr += "    if (a{0} == {1}) l{0} = 0;\n".format(1, (n-1)//2**(k-1))
            segstr += "    else\n"
        segstr += '    l1 = geq(x, cb1);\n'

        for l in range(2, k):
            segstr += "    a%d = (a%d << 1) | l%d;\n" % (l, l-1, l-1)
            segstr += "    cb%d = (excpt, b%d[a%d]);\n" % (l, l, l)
            if ((n-1)//2**(k-1-l) % 2 == 0):
                segstr += "    if (a{0} == {1}) l{0} = 0;\n".format(l, (n-1)//2**(k-l))
                segstr += "    else\n"
            segstr += "    l%d = geq(x, cb%d);\n" % (l, l)

        for l in range(0, k):
            segstr += "    addr.set_bit(%d, l%d);\n" % (k-l-1, l)
        segstr += '    return addr;\n}\n\n'
        f.write(segstr)

def print_coef_rom(spec, logfname, seglist_in):
    slist = seglist_in[5:]
    n = len(slist)
    scale_list = [s.p_scale for s in slist if s.p_scale!='']
    if scale_list:
        max_scale = max(scale_list)
    else:
        logging.debug(' print coef scale_list is false')
        max_scale = -1
    logging.info(' max p_scale is %d', max_scale)
    #if max_scale < 0:
    #    scalw = 0
    #else:
    #    scalw = ceil(log2(max_scale+1))
    fixed_intw = max_scale + 2
    fixed_fracw = spec.wf + 1
    fixed_totalw = fixed_intw + fixed_fracw 
    with open(os.path.join(logfname, logfname+'-coef.h'), 'w') as f:
        flagstr = 'static ap_uint<3> const yflagSgn[%d] = {' % n
        expstr = 'static ap_uint<%d> const yexp[%d] = {' % (spec.we, n)
        #skipstr = 'ap_uint<1> skip[%d] = {' % n
        #scalstr = 'ap_uint<%d> scal[%d] = {' % (scalw, n)
        cstr = 'static ap_fixed<{0},{1},AP_TRN,AP_SAT> const coef{2}[{3}] = {{'
        #coeflist = [cstr.format(spec.wf+1-d, 0, d, n) for d in range(0, spec.deg+1)]
        #if spec.no_scale:
        coeflist = [cstr.format(fixed_totalw - d, fixed_intw, d, n) for d in range(0, spec.deg+1)]
        #else:
        #    coeflist = [cstr.format(spec.wf+1-d+1, 1, d, n) for d in range(0, spec.deg+1)]
        for i, cur_s in enumerate(slist):
            if i > 0: 
                flagstr += ','
                expstr += ','
                #skipstr += ','
                #scalstr += ','
                coeflist = [s+',' for s in coeflist]
            flagstr += '0b' + cur_s.y_flag + cur_s.y_sgn
            if cur_s.y_e:
                if cur_s.y_flag != '01':
                    exp = 0;
                else:
                    exp = int(cur_s.y_e)+2**(spec.we-1)-1
                #skipstr += '0x0'
                p = cur_s.poly
                div = 1
                for d in range(0, spec.deg+1):
                    if d > 1:
                        div = div * d
                        coeflist[d] += sollya.eval('display=decimal!;p=%s;p(0)/%d;' % (p, div), log=False)
                    else:
                        coeflist[d] += sollya.eval('display=decimal!;p=%s;p(0);' % p, log=False)
                    p = "diff(%s)" % p
            else:
                exp = 2**(spec.we-1)-1
                #skipstr += '0x1'
                coeflist = [s+'0' for s in coeflist]
            expstr += str(exp)
            
            #if cur_s.p_scale:
            #    scalstr += str(cur_s.p_scale)
            #else:
            #    scalstr += '0'

            if (i+1) % 16 == 0:
                flagstr += '\n'
                expstr += '\n'
                #skipstr += '\n'
                #scalstr += '\n'
                coeflist = [s+'\n' for s in coeflist]
        flagstr += '};\n'
        expstr += '};\n'
        #skipstr += '};\n'
        #scalstr += '};\n'
        coeflist = [s+'};\n' for s in coeflist]
        #f.write('#include "%s.h"\n\n' % logfname)
        f.write(flagstr)
        f.write(expstr)
        #f.write(skipstr)
        #if not spec.no_scale:
        #    f.write(scalstr)
        for s in coeflist:
            f.write(s)
    #with open(logfname+'.h', 'a') as f:
    #    estr = 'extern ap_uint<3> yflagSgn[%d];\n' % n
    #    estr += 'extern ap_uint<%d> yexp[%d];\n' % (spec.we, n)
    #    cstr = 'extern ap_fixed<{0},{1},AP_TRN,AP_SAT> coef{2}[{3}];\n'
    #    for d in range(0, spec.deg+1):
    #        estr += cstr.format(fixed_totalw - d, fixed_intw, d, n)
    #    f.write(estr+'\n')

def print_nncore_top(spec, logfname, seglist_in):
    # skip NaN, which is row 0
    specials = seglist_in[1:5]
    slist = seglist_in[5:]
    scale_list = [s.p_scale for s in slist if s.p_scale!='']
    if scale_list:
        max_scale = max(scale_list)
    else:
        logging.debug(' print top scale_list is false')
        max_scale = -1
    #if spec.no_scale:
    #    guardbits = 0
    #else:
    #    guardbits = max_scale
    fixed_intw = max_scale + 2
    with open(os.path.join(logfname, logfname+'.h'), 'a') as f:
        topstr = (
            'template<int WET, int WFT> ap_uint<3 + WET + WFT> nncore_top(\n'
            '        ap_uint<3 + WET + WFT> &x) {\n'
            '#pragma HLS INLINE\n'
            '    ap_uint<3> excpt;\n'
            '    ap_uint<ADDRW> addr;\n'
            '    ap_uint<3 + WET + WFT> y;\n'
            '    ap_uint<2> xflag;\n'
            '    ap_uint<1> xsgn;\n'
            '    ap_uint<WET> xe;\n'
            '    ap_uint<WFT> xsig;\n'
            '    ap_uint<1> zero = 0;\n'
            '    ap_uint<3 + WET + WFT> xabs;\n'
            '    ap_ufixed<WFT, 0, AP_TRN, AP_SAT> xfix, yfix;\n'
        )
        for d in range(spec.deg):
            #if not spec.no_scale:
            #    topstr += '    ap_fixed<1 + WFT+1 + {0+1}, 1, AP_TRN, AP_SAT> a{1};\n'.format(guardbits, d)
            #else:
            topstr += '    ap_fixed<{0} + WFT, {1}, AP_TRN, AP_SAT> a{2};\n'.format(fixed_intw + d + 1, fixed_intw, d)
        topstr += (
            '    excpt = x.range(2 + WET + WFT, WET + WFT);\n'
            '    xflag = x.range(2 + WET + WFT, 1 + WET + WFT);\n'
            '    xsgn = x[WET + WFT];\n'
            '    xe = x.range(WET + WFT - 1, WFT);\n'
            '    xsig = x.range(WFT - 1, 0);\n'
            '    xabs = (xflag, zero, xe, xsig);\n'
            '    switch (excpt) {\n'
            '    case 0b110:\n'
            '    case 0b111:\n'
            '        y = ap_uint<3 + WET + WFT>(0x3) << (1 + WET + WFT);\n'
            '        break;\n')
        excptDict = { 
            '-infty': '    case 0b101:\n',
            '-0'    : '    case 0b001:\n',
            '0'     : '    case 0b000:\n',
            'infty' : '    case 0b100:\n',}
        for s in specials:
            topstr += excptDict[s.x_lo]
            decstr = sollya.eval("display=decimal!;%s;" % s.x_lo_y)
            args = [str(spec.we), str(spec.wf), decstr]
            topstr += ('        y = ap_uint<3 + WET + WFT>("%s", 2);\n' % 
                       run_fp2bin(args).split('\n')[0])
            topstr += '        break;\n'
        topstr += '    default:\n'
        skiplist = ['addr == %d'%i for i, s in enumerate(slist) if not s.y_e]
        skipstr = ' || '.join(skiplist + ['false'])
        parDict = {
            Parity.NO : (
                '        addr = segmentor(x);\n'
                '#ifdef debug\n'
                '        cout << " addr is " << addr << endl;\n'
                '#endif\n' +
                '        if (%s) {\n' % skipstr +
                '            y = x;\n'
                '        } else {\n'
                '            y.range(2 + WET + WFT, WET + WFT) = yflagSgn[addr];\n'),
            Parity.ODD : (
                '        addr = segmentor(xabs);\n'
                '#ifdef debug\n'
                '        cout << " addr is " << addr << endl;\n'
                '#endif\n' +
                '        if (%s) {\n' % skipstr +
                '            y = x;\n'
                '        } else {\n'
                '            y.range(2 + WET + WFT, 1 + WET + WFT) = yflagSgn[addr].range(2, 1);\n'
                '            y[WET + WFT] = xsgn;\n'),
            Parity.EVEN : (
                '        addr = segmentor(xabs);\n'
                '#ifdef debug\n'
                '        cout << " addr is " << addr << endl;\n'
                '#endif\n' +
                '        if (%s) {\n' % skipstr +
                '            y = x;\n'
                '        } else {\n'
                '            y.range(2 + WET + WFT, 1 + WET + WFT) = yflagSgn[addr].range(2, 1);\n'
                '            y[WET + WFT] = 0;\n'),
        }
        topstr += parDict[spec.par]
        topstr += (
            '            y.range(WET + WFT - 1, WFT) = yexp[addr];\n'
            '            xfix.range(WFT - 1, 0) = xsig;\n'
        )
        for d in range(spec.deg-1, -1, -1):
            if d == spec.deg-1:
                topstr += ('            a{0} = coef{0}[addr] + xfix * coef{1}[addr];\n'
                           '#ifdef debug\n'
                           '            cout << " a{0} is " << a{0};\n'
                           '            cout << " coef{0} is " << coef{0}[addr];\n'
                           '            cout << " coef{1} is " << coef{1}[addr];\n'
                           '            cout << " xfix is " << xfix << endl;\n'
                           '#endif\n'
                           ).format(d, d+1)
            else:
                topstr += ('            a{0} = coef{0}[addr] + xfix * a{1};\n'
                           '#ifdef debug\n'
                           '            cout << " a{0} is " << a{0};\n'
                           '            cout << " coef{0} is " << coef{0}[addr];\n'
                           '            cout << " a{1} is " << a{1};\n'
                           '            cout << " xfix is " << xfix << endl;\n'
                           '#endif\n'
                           ).format(d, d+1)
        #if not spec.no_scale:
        #    topstr += (
        #        '            if (scal[addr] == 0) {\n'
        #    )
        topstr += (
            '                yfix = a0;\n'
            '                y.range(WFT - 1, 0) = yfix.range(WFT - 1, 0);\n'
        )
        #if not spec.no_scale:
        #    topstr += (
        #        '            } else {\n'
        #        '                yfix = a0 << scal[addr];\n'
        #        '                y.range(WFT - 1, 0) = yfix.range(WFT - 1, 0);\n'
        #        '            }\n'
        #    )
        topstr += (
            '        }\n'
            '        break;\n'
            '    }\n'
            '    return y;\n'
            '}\n\n'
        )
        topstr += 'ap_uint<{0}> nncore(ap_uint<{0}> &x);\n\n'.format(spec.we+spec.wf+3)
        topstr += '#endif\n\n'
        f.write(topstr)
    with open(os.path.join(logfname, logfname+'.cpp'), 'w') as f:
        cstr = '\n#include "{0}.h"\n\n'.format(logfname)
        cstr += ('ap_uint<{0}> nncore(ap_uint<{0}> &x) {{\n'
                 '#pragma HLS PIPELINE II=1\n'
                 '    return nncore_top<{1}, {2}>(x);\n'
                 '}}\n\n'
                 ).format(spec.we+spec.wf+3, spec.we, spec.wf)
        f.write(cstr)

def print_test_data(spec, logfname, seglist_in):
    slist = seglist_in[5:]
    with open(os.path.join(logfname, logfname+'-test.dat'), 'w') as f:
        for s in slist:
            for p in s.testPoints(spec):
                f.write(p)

def print_testbench(spec, logfname):
    with open(os.path.join(logfname, logfname+'-test.cpp'), 'w') as f:
        cstr = (
            '#include <iostream>\n'
            '#include <fstream>\n'
            '#include <sstream>\n'
            '#include "{logfname}.h"\n\n'
            'using namespace std;\n\n'
            'int main() {{\n'
            '    int retval = 0;\n'
            '    int failcount = 0;\n'
            '    string line;\n'
            '    string xhex, yhex, xdec, ydec, xbin, ybin;\n'
            '    ap_uint<{wt}> x, ytest, ygold;\n'
            '    ap_uint<{we}> ytestexp, ygoldexp;\n'
            '    ap_uint<{wf}> ytestsig, ygoldsig;\n\n'
            '    cout << setprecision({wf}) << hex;\n'
            '    ifstream infile("{logfname}-test.dat");\n\n'
            '    if (infile.is_open()) {{\n'
            '        while (getline(infile, line)) {{\n'
            '            istringstream iss(line);\n'
            "            getline(iss, xbin, ',');\n"
            "            getline(iss, ybin, ',');\n"
            "            getline(iss, xhex, ',');\n"
            "            getline(iss, yhex, ',');\n"
            #"            getline(iss, xdec, ',');\n"
            #"            getline(iss, ydec, ',');\n"
            '            x = ap_uint<{wt}>(xbin.c_str(), 2);\n'
            '            ygold = ap_uint<{wt}>(ybin.c_str(), 2);\n'
            '            ytest = nncore(x);\n'
            '            if (ytest != ygold) {{\n'
            '                ytestexp = ytest.range({msbe}, {wf});\n'
            '                ygoldexp = ygold.range({msbe}, {wf});\n'
            '                ytestsig = ytest.range({msbf}, 0);\n'
            '                ygoldsig = ygold.range({msbf}, 0);\n'
            '                ap_uint<{wf}> sigdiff = (ytestsig > ygoldsig)? ytestsig - ygoldsig : \n'
            '                                        ygoldsig - ytestsig;\n'
            '                ap_uint<{we}> expdiff = (ytestexp > ygoldexp)? ytestexp - ygoldexp : \n'
            '                                        ygoldexp - ytestexp;\n'
            '                if ((expdiff == 0 && sigdiff > 1) || (expdiff > 0)) {{\n'
            '                    cout << "Match failed,     Test: " << ytest << ", Gold: " << ygold;\n'
            '                    cout << ", sigdiff: " << sigdiff << ", expdiff: " << expdiff;\n'
            '                    retval = 1;\n'
            '                    failcount += 1;\n'
            '                }} else {{\n'
            '                    cout << "Matched faithful, Test: " << ytest << ", Gold: " << ygold;\n'
            '                }}\n'
            '                cout << ", Input bin: " << x;\n'
            '                cout << ", hex: " << xhex << endl;\n'
            '            }} else {{\n'
            '                cout << "Matched exact,    Test: " << ytest << ", Gold: " << ygold << endl;\n'
            '            }}\n'
            '        }}\n'
            '    }}\n'
            '    if (retval != 0) {{\n'
            '        printf("Test failed !!!\\n");\n'
            '        cout << "Fail count: " << dec << failcount << endl;\n'
            '    }} else {{\n'
            '        printf("Test passed !\\n");\n'
            '    }}\n'
            '    return retval;\n'
            '}}\n'
        ).format(logfname=logfname, we=spec.we, wf=spec.wf, 
                 wt=spec.we+spec.wf+3, msbf=spec.wf-1, msbe=spec.we+spec.wf-1)
        f.write(cstr)

def print_tcl(spec, logfname):
    with open(os.path.join(logfname, logfname+'.tcl'), 'w') as f:
        tstr = ('open_project -reset proj_{logfname}\n\n'
                'add_files {logfname}.cpp\n'
                #'add_files {logfname}-coef.cpp\n'
                'add_files -tb {logfname}-test.cpp\n'
                'add_files -tb {logfname}-test.dat\n\n'
                'set_top nncore\n\n'
                'open_solution -reset solution1\n'
                'set_part {{xc7vx690tffg1157-2}}\n'
                'create_clock -period 4\n\n'
                'source x_hls.tcl\n'
                'csim_design\n'
                'if {{$hls_exec == 1}} {{\n'
                '  csynth_design\n'
                '}} elseif {{$hls_exec == 2}} {{\n'
                '  csynth_design\n'
                '  cosim_design\n'
                '}} elseif {{$hls_exec == 3}} {{\n'
                '  csynth_design\n'
                '  cosim_design\n'
                '  export_design -evaluate verilog -format ip_catalog\n'
                '}} else {{\n'
                '  csynth_design\n'
                '}}\n\n'
                'exit\n'
                ).format(logfname=logfname)
        f.write(tstr)
    with open(os.path.join(logfname, 'x_hls.tcl'), 'w') as f:
        tstr = (
            '# Set to 0: to run setup\n'
            '# Set to 1: to run setup and synthesis\n'
            '# Set to 2: to run setup, synthesis and RTL simulation\n'
            '# Set to 3: to run setup, synthesis, RTL simulation and RTL synthesis\n'
            '# Any other value will run setup only\n'
            'set hls_exec 0\n')
        f.write(tstr)

def find_segments_and_create_arch(args, logfname):
    funcDict = {
        'sigmoid'   : ('f = 1/(1+exp(-x));', '-log(1/x-1)', Mono.INC, Parity.NO),
        'tanh'      : ('f = tanh(x);', 'atanh(x)', Mono.INC, Parity.ODD),
        'd_sigmoid' : ('f = exp(x)/(exp(x)+1)^2;', 
                       'log((x*(-2) + (1 + x*(-4))^(1/2) + 1)/(x*2))', 
                       Mono.DEC, Parity.EVEN),
        'd_tanh'    : ('f = 1-tanh(x)^2;', 'atanh((1-x)^(1/2))', Mono.DEC, Parity.EVEN),
        'atan'      : ('f = atan(x);', '', None, Parity.ODD),
        'd_atan'    : ('f = 1/(x^2+1);', '', None, Parity.EVEN),
        'softsign'  : ('f = x/(abs(x)+1);', '', None, Parity.ODD),
        'd_softsign': ('f = 1/(abs(x)+1)^2;', '', None, Parity.EVEN),
        'softplus'  : ('f = log(1+exp(x));', '', None, Parity.NO),
    }
    if args.dumpfile is None:
        spec = FuncSpec(funcDict[args.func], args)
        transDict0 = {
            'x_u2f' : "x_u2f = (-1)^(%s)*(2^(%s))*(1+x);\n",
            'x_f2u' : "x_f2u = (-1)^(%s)*(1/2^(%s))*x - 1;\n",
            'y_f2u' : "y_f2u = (-1)^(%s)*(1/2^(%s))*x - 1;\n",
        }
        transDict1 = {
            'x_u2f' : "x_u2f = (-1)^(%s)*(2^(%s))*x;\n",
            'x_f2u' : "x_f2u = (-1)^(%s)*(1/2^(%s))*x;\n",
            'y_f2u' : "y_f2u = (-1)^(%s)*(1/2^(%s))*x;\n",
        }
        spec.setTransDict(transDict0)

        pklpath = os.path.join(logfname, logfname + '.pkl')
        with open(pklpath, 'wb') as outfile:
            pickle.dump(spec, outfile, pickle.HIGHEST_PROTOCOL)
            startTime = timer()
            seglist1 = create_seg_by_binade(spec)
            seglist2 = split_seg_by_range_binade(spec, seglist1)
            seglist3 = merge_seg_by_cond(spec, seglist2, y_cond)
            seglist4 = split_seg_by_real_poly(spec, seglist3, '0x1p-%s'%(spec.wf+3))
            seglist5 = split_seg_by_fix_poly(spec, seglist4, '0x1p-%s'%(spec.wf+2))
            pickle.dump(seglist5, outfile, pickle.HIGHEST_PROTOCOL)
            seglist6 = split_outliers(spec, seglist5)
            seglist7 = merge_seg_by_cond(spec, seglist6, poly_cond)
            seglist8 = merge_seg_by_cond(spec, seglist7, mirror_cond)
            endTime = timer()
            elapsed = endTime - startTime
            logging.info(' elasped time in segment algo: %s min %s sec', 
                         str(int(elapsed/60)), str(int(elapsed%60)))
            print_segmentor_rom(spec, logfname, seglist8)
            print_coef_rom(spec, logfname, seglist8)
            print_nncore_top(spec, logfname, seglist8)
            print_test_data(spec, logfname, seglist8)
            print_testbench(spec, logfname)
            print_tcl(spec, logfname)
    else:
        with open(args.dumpfile, 'rb') as infile:
            spec = pickle.load(infile)
            seglist5 = pickle.load(infile)
            seglist6 = split_outliers(spec, seglist5)
            seglist7 = merge_seg_by_cond(spec, seglist6, poly_cond)
            seglist8 = merge_seg_by_cond(spec, seglist7, mirror_cond)
            print_segmentor_rom(spec, logfname, seglist8)
            print_coef_rom(spec, logfname, seglist8)
            print_nncore_top(spec, logfname, seglist8)
            print_test_data(spec, logfname, seglist8)
            print_testbench(spec, logfname)
            print_tcl(spec, logfname)

def main():
    parser = argparse.ArgumentParser(description=
                                     'NnCore: a Neural-network arithmetic Core generator')
    #group = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument('func', choices=['cmp_cndt_code', 'cmp_lss', 'cmp_leq', 'relu', 
                                         'relu6', 'tanh', 'sigmoid', 'd_tanh', 'd_sigmoid',
                                         'atan', 'd_atan', 'softsign', 'd_softsign', 'softplus'], 
                        help='the target function to be generated')
    parser.add_argument('-f', '--dumpfile', required=False,
                        help=('To skip segment algo and read from previous dump file, '
                              'all other optional arguments are ignored when this is set'))
    parser.add_argument('--we', default=5, type=int, choices=range(4,17), required=False,
                        help='Exponent width (range 4-16)', metavar='exp_width')
    parser.add_argument('--wf', default=10, type=int, choices=range(4,65), required=False,
                        help='Mantissa width (range 4-64)', metavar='man_width')
    parser.add_argument('-d', '--deg', default=3, type=int, required=False, 
                        help='Maximum power of the minimax polynomial (default: 3)')
    parser.add_argument('--odd', action='store_true', required=False, 
                        help='To only use odd powers for the minimax polynomial (default: False)')
    parser.add_argument('-c', '--const', default=6.0, type=float, required=False, 
                        help='Constant compared in ReLU6 (default: 6.0)')
#    parser.add_argument('-p', '--pipe', default=0, type=int, choices=range(0, 2), required=False, 
#                        help='Pipeline depth, 0 for combinational, maximum varies for each function', 
#                        metavar='depth')
#    parser.add_argument('--no_mono', action='store_true', required=False, 
#                        help='Disable monotonicity check, debug feature (default: False)')
#    parser.add_argument('--approx_err', default=1.0, type=float, required=False, 
#                        help='The accepted approx. error in ULP (default: 1.0)')
    args = parser.parse_args()
    if args.dumpfile is None:
        logfname = args.func + '-wf%dd%d'%(args.wf,args.deg) + time.strftime("-%Y%m%d-%H%M") 
        #logpath = logfname + '.log'
        os.makedirs(logfname)
        logpath = os.path.join(logfname, logfname + '.log')
    else:
        logfname = args.func + '-f' + time.strftime("-%Y%m%d-%H%M") 
        os.makedirs(logfname)
        logpath = os.path.join(logfname, logfname + '.log')
    logging.basicConfig(filename=logpath, format='%(levelname)s:%(message)s', 
                        level=logging.DEBUG)
    for arg, value in sorted(vars(args).items()):
        logging.info("Argument %s: %r", arg, value)
    action = {
        'relu'          : (nothing, (args,)),
        'relu6'         : (nothing, (args,)),
        'cmp_cndt_code' : (nothing, (args,)),
        'cmp_lss'       : (nothing, (args,)),
        'cmp_leq'       : (nothing, (args,)),
        'sigmoid'       : (find_segments_and_create_arch, (args, logfname)),
        'tanh'          : (find_segments_and_create_arch, (args, logfname)),
        'd_tanh'        : (find_segments_and_create_arch, (args, logfname)),
        'd_sigmoid'     : (find_segments_and_create_arch, (args, logfname)),
        'atan'          : (find_segments_and_create_arch, (args, logfname)),
        'd_atan'        : (find_segments_and_create_arch, (args, logfname)),
        'softsign'      : (find_segments_and_create_arch, (args, logfname)),
        'd_softsign'    : (find_segments_and_create_arch, (args, logfname)),
        'softplus'      : (find_segments_and_create_arch, (args, logfname)),
    }
    fn, params = action.get(args.func, (nothing, (args,)))
    fn(*params)

if __name__ == '__main__':
    main()


